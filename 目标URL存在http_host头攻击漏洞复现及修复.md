# 目标URL存在http_host头攻击漏洞复现及修复

[TOC]

## 漏洞说明

### 漏洞描述

为了方便的获得网站域名，开发人员一般依赖于HTTP Host header。例如，在php里用_SERVER["HTTP_HOST"]。但是这个header是不可信赖的，如果应用程序没有对host header值进行处理，就有可能造成恶意代码的传入。

### 危险等级

中危

### 修复建议

web 应用程序应该使用 SERVER_NAME 而不是 host header。在 Apache 和 Nginx 里可以通过设置一个虚拟机来记录所有的非法 host header。在 Nginx 里还可以通过指定一个 SERVER_NAME 名单，Apache 也可以通过指定一个 SERVER_NAME 名单并开启UseCanonicalName 选项。

下图为绿盟扫描器的报告截图：

![image-20220218104357708](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/http_host/image-20220218104357708.png)



## 漏洞复现

可以使用 curl 或 BurpSuite 工具修改 Host 请求头，复现测试

### curl测试方法

```bash
curl -h

 -I, --head          Show document info only
 -H, --header <header/@file> Pass custom header(s) to server
 -v, --verbose       Make the operation more talkative
```

```bash
curl -v -H "Host: www.baidu.com" http://192.168.64.149
```

测试结果：任意修改 Host 请求头，服务器都能返回 302 Found

![image-20220221204539094](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/http_host/image-20220221204539094.png)

### BurpSuite测试方法

BurpSuite 下载链接：

https://portswigger.net/burp/releases/professional-community-2022-1-1?requestededition=community

开启代理抓包后，Send to Repeater

![image-20220221205725799](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/http_host/image-20220221205725799.png)

点击 Send 重复发送当前包，下面是未修改的正确 Host 请求头，及其返回状态码：200 OK

![image-20220221210328160](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/http_host/image-20220221210328160.png)

任意修改 Host 请求头，再发送数据包，观察返回状态码。可以看到都能返回：200 OK，即正常请求到 web 页面

![image-20220221210651911](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/http_host/image-20220221210651911.png)



## 漏洞修复

### 漏洞修复方案

修改 nginx.conf 配置，设置 server_name，添加正则表达式匹配 host 请求头的值，不匹配情况返回403。

```bash
#       server_name             localhost;

#### http_Host fix
        server_name             127.0.0.1 192.168.64.149;
        if ($http_Host !~* ^127.0.0.1$|^192.168.64.149$) {
                return          403;
        }
                
##  语义解析：
##  ! 非
##  ~ 匹配正则
##  * 任意
##  ^ 匹配开头字符
##  & 匹配末尾字符
##  | 或
##  首先指定server_name字段值；
##  如果变量$http_Host不是（127.0.0.1或192.168.64.149），if判断成立，执行后面的内容，即返回403跳转页面；
##  如果变量$http_Host是（127.0.0.1或192.168.64.149），if判断不成立，则不执行后面的内容。
##  建议查阅nginx官方文档：https://nginx.org/en/docs/http/ngx_http_core_module.html#server_name
```

![image-20220222100802203](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/http_host/image-20220222100802203.png)

重启 nginx

```bash
nginx -s reload
```

![image-20220221212140472](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/http_host/image-20220221212140472.png)

### 漏洞修复验证

当 Host 请求头正确匹配 server_name(127.0.0.1或者192.168.64.149)时，服务器返回了 302 Found；

其他任意 Host 请求头未匹配 server_name 的情况下，服务器都返回 403 Forbidden。

- curl 测试结果

![image-20220221212737864](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/http_host/image-20220221212737864.png)

- BurpSuite 测试结果

![image-20220221213811289](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/http_host/image-20220221213811289.png)

![image-20220221213904723](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/http_host/image-20220221213904723.png)



**以上，如有错误之处，敬请指正。**



## 参考文章

https://nginx.org/en/docs/http/ngx_http_core_module.html#server_name

https://www.freesion.com/article/18441013466/

https://www.cnblogs.com/huiy/p/13427401.html

