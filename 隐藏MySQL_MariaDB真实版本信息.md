# 隐藏MySQL/MariaDB真实版本信息

项目中，甲方对服务器做安全扫描，发现存在较多 MySQL 低版本漏洞，并要求修复。但考虑到业务的影响，不方便直接对当前版本做升级，且我们业务部署在内网环境，也做了网络访问限制，因此实际安全隐患较小。这里尝试寻找其他的方法进行规避，即修改 MySQL 的版本号，隐藏其真实的版本信息。

测试过程记录如下，==**风险操作，仅供参考**==。



## 探测MySQL/MariaDB版本信息

查看当前版本的方法有很多，常见的有：

- 服务器上执行 `mysql -V`

- 登录mysql，使用sql语句查询 

```bash
mysql -uroot -p
> select version();
```

没有登录权限的情况下，还可以使用 telnet 或 nmap 工具进行探测扫描，得到 mysql 的版本信息：

- telnet

```bash
telnet [host] [port]
```

- nmap

```bash
nmap -T4 -sC -sV -p [port] [ip]
```



## 隐藏MySQL真实版本信息

### 测试环境

| 序号 | 名称          | 版本       | 备注               |
| ---- | ------------- | ---------- | ------------------ |
| 1    | Linux操作系统 | CentOS 7.6 | IP：192.168.64.147 |
| 2    | MySQL数据库   | 5.7.22     | rpm安装            |

### 安装过程

1. 下载安装 rpm 包

```bash
yum -y install wget telnet nmap net-tools
wget -P /opt/mysql https://cdn.mysql.com/archives/mysql-5.7/mysql-5.7.22-1.el7.x86_64.rpm-bundle.tar
tar -xf /opt/mysql/mysql-5.7.22-1.el7.x86_64.rpm-bundle.tar -C /opt/mysql
cd /opt/mysql
ls -lh /opt/mysql/
yum -y remove mariadb*
yum -y install \
  mysql-community-common-5.7.22-1.el7.x86_64.rpm \
  mysql-community-libs-5.7.22-1.el7.x86_64.rpm \
  mysql-community-client-5.7.22-1.el7.x86_64.rpm \
  mysql-community-server-5.7.22-1.el7.x86_64.rpm
```

![image-20220304234312333](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304234312333.png)

![image-20220307103257232](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307103257232.png)

![image-20220307104122222](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307104122222.png)

2. 开启 MySQL 服务，添加防火墙策略

```bash
systemctl start mysqld && systemctl status mysqld
firewall-cmd --permanent --add-port=3306/tcp
firewall-cmd --reload
firewall-cmd --list-all
```

![image-20220307104236358](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307104236358.png)

3. 设置密码

```bash
passwd=`grep 'password' /var/log/mysqld.log|awk '{print $NF}'`
mysqladmin -u root -p$passwd password Aa123456.
```

![image-20220307104839658](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307104839658.png)

4. 设置远程连接

```bash
mysql -uroot -pAa123456.
mysql> use mysql;
mysql> select user,host from user;
mysql> update user set host='%' where user='root';
mysql> flush privileges;
mysql> select user,host from user;
```

![image-20220307105056030](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307105056030.png)

5. 查看当前版本

```bash
mysql> select version();
mysql> \q
mysql -V
```

![image-20220307105214557](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307105214557.png)

### 版本信息探测

```bash
telnet 192.168.64.147 3306
nmap -T4 -sC -sV -p 3306 192.168.64.147
```

![image-20220307105320398](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307105320398.png)

### 修改版本信息

1. 备份文件

```bash
cp /usr/bin/mysql /usr/bin/mysql.bak
cp /usr/sbin/mysqld /usr/sbin/mysqld.bak
```

![image-20220307105603753](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307105603753.png)

2. 编辑二进制文件，替换其中的版本信息

==**如果只想对外部探测 MySQL 的真实版本信息进行隐藏，只需改 /usr/sbin/mysqld 文件。**==

:exclamation::exclamation::exclamation:==**注意：版本号不可为空或删减其他信息，否则可能导致服务无法启用！**==

```bash
vi /usr/bin/mysql
# 搜索关键字“Linux”快速定位，修改版本号，建议改为官网最新的稳定版本
^@fwrite^@\0^@\t^@\n^@\\^@ERROR^@ %d (%s)^@ at line %lu^@ in file: '%s'^@ERROR %d (%s): ^@ERROR %d: ^@ERROR: ^@UNKNOWN USER^@MysqlClient^@LOGNAME^@LOGIN^@SUDO_USER^@Linux^@5.7.22^@x86_64^@Show warnings enabled.^@Show warnings disabled.^@Aborted^@Bye^@Writing history-file %s

# 参考案例：
^@fwrite^@\0^@\t^@\n^@\\^@ERROR^@ %d (%s)^@ at line %lu^@ in file: '%s'^@ERROR %d (%s): ^@ERROR %d: ^@ERROR: ^@UNKNOWN USER^@MysqlClient^@LOGNAME^@LOGIN^@SUDO_USER^@Linux^@5.7.37^@x86_64^@Show warnings enabled.^@Show warnings disabled.^@Aborted^@Bye^@Writing history-file %s
```

![image-20220307105906292](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307105906292.png)

![image-20220307110043013](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307110043013.png)

![image-20220307110424718](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307110424718.png)

```bash
vi /usr/sbin/mysqld
# 搜索关键字“--language”快速定位，修改版本号，建议改为官网最新的稳定版本
H<8b>^P1öéz¬,ÿ1Òëõ^@^@H<83>ì^HH<83>Ä^HÃ^@^@^@^@^@^@^@^@^@^@^@^A^@^B^@^@^@^@^@^@^@^@^@^@^@^@^@%s: %s^@MYSQL_AUDIT_GENERAL_ERROR^@OFF^@BOTH^@NO_AUTO_CLEAR^@NO_INTERNAL_LOOKUP^@Unknown^@tcp^@MYSQL_TCP_PORT^@/var/lib/mysql/mysql.sock^@MYSQL_UNIX_PORT^@Unable to delete pid file: %s^@%.3f^@.lower-test^@.LOWER-TEST^@Can't create test file %s^@Shutting down slave threads^@'--lc-messages-dir'^@--language/-l^@--binlog_max_flush_queue_time^@5.7.22^@Linux^@%s  Ver %s for %s on %s (%s)

# 参考案例：
H<8b>^P1öéz¬,ÿ1Òëõ^@^@H<83>ì^HH<83>Ä^HÃ^@^@^@^@^@^@^@^@^@^@^@^A^@^B^@^@^@^@^@^@^@^@^@^@^@^@^@%s: %s^@MYSQL_AUDIT_GENERAL_ERROR^@OFF^@BOTH^@NO_AUTO_CLEAR^@NO_INTERNAL_LOOKUP^@Unknown^@tcp^@MYSQL_TCP_PORT^@/var/lib/mysql/mysql.sock^@MYSQL_UNIX_PORT^@Unable to delete pid file: %s^@%.3f^@.lower-test^@.LOWER-TEST^@Can't create test file %s^@Shutting down slave threads^@'--lc-messages-dir'^@--language/-l^@--binlog_max_flush_queue_time^@5.7.37^@Linux^@%s  Ver %s for %s on %s (%s)
```

![image-20220307110703144](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307110703144.png)

![image-20220307110813803](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307110813803.png)

![image-20220307110848382](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307110848382.png)

3. 重启服务

```bash
systemctl restart mysqld
systemctl status mysqld
netstat -nplt |grep mysqld
```

![image-20220307111103922](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307111103922.png)

4. 确认版本

```bash
mysql -V
mysql -h 192.168.64.147 -uroot -pAa123456.
mysql> select version();
mysql> \q
```

![image-20220307111847330](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307111847330.png)

5. 其他业务相关的测试

### 版本信息复测

```bash
telnet 192.168.64.147 3306
nmap -T4 -sC -sV -p 3306 192.168.64.147
```

![image-20220307111949871](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220307111949871.png)



## 隐藏MariaDB真实版本信息

### 测试环境

| 序号 | 名称          | 版本       | 备注               |
| ---- | ------------- | ---------- | ------------------ |
| 1    | Linux操作系统 | CentOS 7.6 | IP：192.168.64.147 |
| 2    | MariaDB数据库 | 5.5.68     | rpm安装            |

### 安装过程

1. yum安装

```bash
yum -y install mariadb-server telnet nmap net-tools
```

![image-20220304200302311](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304200302311.png)

2. 开启 MySQL 服务，添加防火墙策略

```bash
systemctl start mariadb && systemctl status mariadb
firewall-cmd --permanent --add-port=3306/tcp
firewall-cmd --reload
firewall-cmd --list-all
```

![image-20220304200406787](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304200406787.png)

3. 设置密码和远程连接


```bash
mysqladmin -u root password "Aa123456."
mysql -uroot -pAa123456.
MariaDB [(none)]> use mysql;
MariaDB [mysql]> select user,host from user;
MariaDB [mysql]> update user set host='%' where user='root';
MariaDB [mysql]> flush privileges;
MariaDB [mysql]> select user,host from user;
```

![image-20220304205001813](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304205001813.png)

![image-20220304205114689](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304205114689.png)

4. 查看当前版本


```bash
MariaDB [mysql]> select version();
MariaDB [mysql]> \q
mysql -V
```

![image-20220304205655345](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304205655345.png)

### 版本信息探测

```bash
telnet 192.168.64.147 3306
nmap -T4 -sC -sV -p 3306 192.168.64.147
```

![image-20220304210101730](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304210101730.png)

### 修改版本信息

1. 备份文件

```bash
cp /usr/bin/mysql /usr/bin/mysql.bak
cp /usr/libexec/mysqld /usr/libexec/mysqld.bak
```

![image-20220304210557680](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304210557680.png)

2. 编辑二进制文件，替换其中的版本信息

==**如果只想对外部探测 MariaDB 的真实版本信息进行隐藏，只需改 /usr/libexec/mysqld 文件。**==

:exclamation::exclamation::exclamation:==**注意：版本号不可为空或删减其他信息，否则可能导致服务无法启用！**==

```bash
vi /usr/bin/mysql
# 搜索关键字“-MariaDB”快速定位，修改版本号，建议改为官网最新的稳定版本
^@KILL %s%lu^@ERROR^@ at line %lu^@ in file: '%s'^@ERROR %d (%s)^@ERROR %d^@Linux^@5.5.68-MariaDB^@readline^@x86_64^@vi^@EDITOR^@VISUAL^@Command '%.40s' failed^@Show warnings disabled.^@Show warnings enabled.^@Usage: \! shell-command^@Charset changed^@Charset is not found^@show databases^@show tables^@%.64s.%.64s^@*** NONE ***^@Connection id:    %lu^@Current database: %.128s

# 参考案例：
^@KILL %s%lu^@ERROR^@ at line %lu^@ in file: '%s'^@ERROR %d (%s)^@ERROR %d^@Linux^@10.7.3-MariaDB^@readline^@x86_64^@vi^@EDITOR^@VISUAL^@Command '%.40s' failed^@Show warnings disabled.^@Show warnings enabled.^@Usage: \! shell-command^@Charset changed^@Charset is not found^@show databases^@show tables^@%.64s.%.64s^@*** NONE ***^@Connection id:    %lu^@Current database: %.128s
```

![image-20220304210730310](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304210730310.png)

![image-20220304210907855](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304210907855.png)

![image-20220304211059834](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304211059834.png)

```bash
vi /usr/libexec/mysqld 
# 同样通过搜索关键字“-MariaDB”快速定位，修改版本号，建议改为官网最新的稳定版本
^@CLOSE_CONNECTION^@unauthenticated^@unconnected^@Error in accept^@5.5.68-MariaDB^@x86_64^@Linux^@%s  Ver %s for %s on %s (%s)

# 参考案例：
^@CLOSE_CONNECTION^@unauthenticated^@unconnected^@Error in accept^@10.7.3-MariaDB^@x86_64^@Linux^@%s  Ver %s for %s on %s (%s)
```

![image-20220304211217553](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304211217553.png)

![image-20220304211304597](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304211304597.png)

3. 重启服务

```bash
systemctl restart mariadb
systemctl status mariadb
netstat -nplt |grep mysqld
```

![image-20220304211800657](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304211800657.png)

4. 确认版本

```bash
mysql -V
mysql -h 192.168.64.147 -uroot -pAa123456.
MariaDB [(none)]> select version();
MariaDB [(none)]> \q
```

![image-20220304212212099](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304212212099.png)

5. 其他业务相关的测试

### 版本信息复测

为防止探测过程中出现错误连接次数太多导致IP被锁定，建议先提高允许的max_connection_errors数量；

或使用清楚缓存的方法，把计数清理掉

```bash
mysql -h 127.0.0.1 -uroot
MariaDB [(none)]> show global variables like '%max_connect_errors%';
MariaDB [(none)]> set global max_connect_errors=1000;
MariaDB [(none)]> show global variables like '%max_connect_errors%';
MariaDB [(none)]> flush hosts;
MariaDB [(none)]> \q
```

![image-20220304214830464](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304214830464.png)

复测结果

```bash
telnet 192.168.64.147 3306
nmap -T4 -sC -sV -p 3306 192.168.64.147
```

![image-20220304215408227](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%9A%90%E8%97%8FMySQL_MariaDB%E7%9C%9F%E5%AE%9E%E7%89%88%E6%9C%AC%E4%BF%A1%E6%81%AF/image-20220304215408227.png)