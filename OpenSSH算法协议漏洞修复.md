# OpenSSH算法协议漏洞修复

由于低版本的OpenSSH使用了过时不安全的加密算法协议，通常OpenSSH在版本迭代更新时会弃用这些不安全的加密算法。

如果我们仍要继续使用旧版本的OpenSSH，可以根据实际情况，考虑屏蔽掉不安全的加密算法，以降低安全风险。



## SSH Weak Key Exchange Algorithms Enabled（启用SSH弱密钥交换算法）

![image-20220519093546828](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519093546828.png)

### 查看kexalgorithms

查看客户端支持的kexalgorithms

```bash
ssh -Q kex
```

![image-20220519101951557](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519101951557.png)

查看服务端支持的kexalgorithms

```bash
sshd -T | grep -w kexalgorithms
```

![image-20220519102045384](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519102045384.png)

### 修复方法

修改sshd_config配置文件，屏蔽掉不安全的KexAlgorithms。其中sshd_config的配置参数说明如下：

- 当前openssh版本支持的算法列表和参数用法可以从帮助文档中查找到。
- 指定可用的KEX (Key Exchange)算法，多个算法之间必须以逗号分隔。
- 另外，如果指定的列表以' + '字符开头，则指定的算法将被追加到默认集，而不是替换原有默认的。
- 如果指定的列表以' - '字符开头，则指定的算法(包括通配符)将从默认集中删除，而不是替换。（演示环境中的openssh7.4不支持）
- 如果指定的列表以' ^ '字符开头，则指定的算法将被放在默认集的开头。（演示环境中的openssh7.4不支持）

```bash
man sshd_config |grep -A 40 -w KexAlgorithms
echo "KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group14-sha256,diffie-hellman-group14-sha1" >> /etc/ssh/sshd_config
systemctl restart sshd
sshd -T | grep -w kexalgorithms
```

![image-20220519102914721](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519102914721.png)

![image-20220519102948288](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519102948288.png)

### 测试验证

客户端指定KexAlgorithms进行连接测试

```bash
ssh -v -oKexAlgorithms=diffie-hellman-group1-sha1 root@192.168.64.101
```

![image-20220519104500142](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519104500142.png)

不指定 KexAlgorithms 连接测试

```bash
ssh -v root@192.168.64.101
```

![image-20220519104816204](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519104816204.png)



## SSH Server CBC Mode Ciphers Enabled（启用SSH服务器CBC模式密码）-CVE-2008-5161

![image-20220519093656295](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519093656295.png)

### 查看ciphers

查看客户端支持的ciphers

```bash
ssh -Q cipher
```

![image-20220519093753716](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519093753716.png)

查看服务端支持的ciphers

```bash
sshd -T | grep -w ciphers
```

![image-20220519102219561](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519102219561.png)

### 修复方法

修改sshd_config配置文件，屏蔽掉不安全的ciphers，重启sshd服务。其中sshd_config的配置参数说明如下：

- 当前openssh版本支持的ciphers列表和参数用法可以从帮助文档中查找到。
- 指定允许使用的ciphers，多个ciphers必须以逗号分隔。
- 如果指定的列表以' + '字符开头，则指定的ciphers将被追加到默认ciphers集，而不是替换默认的ciphers。
- 如果指定的列表以' - '字符开头，则指定的ciphers(包括通配符)将从默认中删除，而不是替换。（演示环境中的openssh7.4不支持）
- 如果指定的列表以' ^ '字符开头，则指定的ciphers将放在默认集的开头。（演示环境中的openssh7.4不支持）

```bash
man sshd_config |grep -A 40 -w Ciphers
echo "Ciphers chacha20-poly1305@openssh.com,aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com" >> /etc/ssh/sshd_config
systemctl restart sshd
sshd -T | grep -w ciphers
```

![image-20220519093927049](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519093927049.png)

![image-20220519102452297](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519102452297.png)

### 测试验证

客户端指定 CBC Mode Ciphers 进行连接测试

```bash
ssh -v -oCiphers=3des-cbc root@192.168.64.101
```

![image-20220519094226359](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519094226359.png)

不指定 Ciphers 连接测试

```bash
ssh -v root@192.168.64.101
```

![image-20220519101531121](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519101531121.png)

### Nessus扫描

修复前扫描结果

![image-20220519110351670](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519110351670.png)

修复后扫描结果

![image-20220519111056603](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220519111056603.png)



## Diffie-Hellman Key Agreement Protocol 资源管理错误漏洞(CVE-2002-20001)

![image-20220517201032288](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/OpenSSH%E7%AE%97%E6%B3%95%E5%8D%8F%E8%AE%AE%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D/image-20220517201032288.png)

与[SSH Weak Key Exchange Algorithms Enabled](#SSH Weak Key Exchange Algorithms Enabled（启用SSH弱密钥交换算法）)中方法类似，屏蔽不安全的KexAlgorithms DH算法



## 附：ssh客户端与服务器命令

```bash
ssh -Q kex							#查看客户端支持的KexAlgorithms列表
ssh -Q cipher						#查看客户端支持的Ciphers列表
sshd -T | grep -w kexalgorithms		#查看服务端支持的KexAlgorithms列表
sshd -T | grep -w ciphers			#查看服务端支持的Ciphers列表
man sshd_config |grep -A 40 -w KexAlgorithms				#查看当前openssh版本支持的KexAlgorithms列表
man sshd_config |grep -A 40 -w Ciphers						#查看当前openssh版本支持的Ciphers列表
ssh -v -oKexAlgorithms={KexAlgorithms} {user}@{ipaddr}		#客户端指定KexAlgorithms进行连接测试
ssh -v -oCiphers={ciphers} {user}@{ipaddr}					#客户端指定Ciphers进行连接测试,并显示debug信息
```

服务端支持的Ciphers和KexAlgorithms列表还可以使用nmap测试查看

```bash
nmap --script ssh2-enum-algos 192.168.64.100
```

支持的Ciphers列表：

![image-20220517211829139](OpenSSH算法协议漏洞修复.assets/image-20220517211829139.png)

支持的KexAlgorithms列表：

![image-20220517210136119](OpenSSH算法协议漏洞修复.assets/image-20220517210136119.png)



## 参考文章

互联网工程任务组官方文档1：https://datatracker.ietf.org/doc/html/draft-ietf-curdle-ssh-kex-sha2-20#page-16

互联网工程任务组官方文档2：https://datatracker.ietf.org/doc/html/rfc8732

openbsd官方帮助文档：https://man.openbsd.org/sshd_config

nmapdoc：https://nmap.org/nsedoc/scripts/ssh2-enum-algos.html

https://github.com/Balasys/dheater
