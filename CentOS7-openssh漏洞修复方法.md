# CentOS7-openssh漏洞修复方法

2021年09月26日发布的[OpenSSH_8.8](https://www.openssh.com/releasenotes.html#8.8)中移除了对RSA-SHA1算法的支持，但由于该算法使用广泛，在OpenSSH_8.8中允许用户通过HostkeyAlgorithms和PubkeyAcceptedAlgorithms选项配置，重新启用对单个目标主机和用户进行RSA-SHA1的连接或认证。

项目实际环境中如果使用了RSA-SHA1，不会轻易进行openssh的版本升级，但这就有可能被甲方的安全扫描发现高危漏洞，并要求修复。对此，才有了以下的应对措施。

## 版本探测

telnet 服务器的ssh端口，回显中可以看到当前的openssh版本

![image-20220517182340017](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7-openssh%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D%E6%96%B9%E6%B3%95/image-20220517182340017.png)

## 修复方法

备份文件

```bash
cp /usr/sbin/sshd /usr/sbin/sshd.bak
ls /usr/sbin/sshd*
```

![image-20220517182526913](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7-openssh%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D%E6%96%B9%E6%B3%95/image-20220517182526913.png)

修改sshd二进制文件中的版本号，并重启sshd服务

==**注意：**版本号的格式有要求，可以查阅[OpenSSH官网](https://www.openssh.com/)，修改为最新的安全版本，这里的测试环境修改为 “OpenSSH_8.8”==

```bash
strings /usr/sbin/sshd |grep OpenSSH_8.7
sed -i 's/OpenSSH_8.7/OpenSSH_8.8/g' /usr/sbin/sshd
strings /usr/sbin/sshd |grep OpenSSH_8.8
systemctl restart sshd
```

![image-20220517182202919](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7-openssh%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D%E6%96%B9%E6%B3%95/image-20220517182202919.png)

## 版本复测

客户端再次 telnet 服务器的ssh端口测试，回显中为修改后的版本即可

![image-20220517182241301](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7-openssh%E6%BC%8F%E6%B4%9E%E4%BF%AE%E5%A4%8D%E6%96%B9%E6%B3%95/image-20220517182241301.png)

## 附：批量处理脚本

```bash
now_ver=`ssh -V 2>&1 |cut -c '1-11'`					 #当前版本
change_ver=OpenSSH_8.8									 #修改版本

cp /usr/sbin/sshd{,.bak}								 #备份文件
strings /usr/sbin/sshd |grep ${now_ver}					 #查看当前二进制文件中的版本号
sed -i "s/${now_ver}/${change_ver}/g" /usr/sbin/sshd	 #修改二进制文件中的版本号
strings /usr/sbin/sshd |grep ${change_ver}				 #检查版本号是否修改成功
systemctl restart sshd									 #重启sshd服务
```

