# k8s集群环境iptables测试

## 测试环境说明

基础环境

| IP             | hostname   | OS        | Kubernetes version |
| -------------- | ---------- | --------- | ------------------ |
| 192.168.64.100 | k8s-master | centos7.6 | v1.16.0            |
| 192.168.64.101 | k8s-node1  | centos7.6 | v1.16.0            |
| 192.168.64.102 | k8s-node2  | centos7.6 | v1.16.0            |

![0.1.k8s环境](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/0.1.k8s%E7%8E%AF%E5%A2%83.png)

nginx-service

![0.nginx-service](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/0.nginx-service.png)

master端口

![0.master_port](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/0.master_port.png)

node端口

![0.node_port](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/0.node_port.png)

## 测试过程

### 启用iptables

业务端口tcp32600测试不通

![1.开启iptables](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/1.%E5%BC%80%E5%90%AFiptables.png)

当前iptables默认规则-master节点

![2.master_iptables](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/2.master_iptables.png)

当前iptables默认规则-node节点

![3.node_iptables](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/3.node_iptables.png)

master节点telnet测试node节点k8s相关服务端口不通

![4.1.master节点telnet测试](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/4.1.master%E8%8A%82%E7%82%B9telnet%E6%B5%8B%E8%AF%95.png)

node节点telnet测试master节点k8s相关服务端口也不通

![4.2.node节点telnet测试](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/4.2.node%E8%8A%82%E7%82%B9telnet%E6%B5%8B%E8%AF%95.png)

### 添加规则

在master，node1，node3同时添加规则

```bash
service iptables save                                    #保存当前规则
cp /etc/sysconfig/iptables /etc/sysconfig/iptables.bak   #备份当前规则
```

直接编辑 /etc/sysconfig/iptables 文件，添加规则及调整顺序

```bash
vi /etc/sysconfig/iptables

#允许集群间主机互访所有端口
-A INPUT -m iprange --src-range 192.168.64.100-192.168.64.102 -j ACCEPT

#允许本机访问所有端口
-A INPUT -s 127.0.0.1 -j ACCEPT

#允许外部访问业务端口
-A INPUT -p tcp --dport 32600 -j ACCEPT

#将默认拒绝规则需要下移至INPUT链最后
-A INPUT -j REJECT --reject-with icmp-host-prohibited

#将默认拒绝规则需要下移至FORWARD链最后
-A FORWARD -j REJECT --reject-with icmp-host-prohibited
```

![image-20211209223449593](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/image-20211209223449593.png)

```bash
systemctl restart iptables             #重启iptables
iptables -nL                           #确认规则，注意顺序
```

![image-20211209224021225](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/image-20211209224021225.png)

### 验证测试

nginx-service业务端口tcp-32600已通

![7.1.调整FORWARD链的规则顺序](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/7.1.%E8%B0%83%E6%95%B4FORWARD%E9%93%BE%E7%9A%84%E8%A7%84%E5%88%99%E9%A1%BA%E5%BA%8F.png)

master节点，内部端口测试已通

![8.1.容器内部端口测试已通](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/8.1.%E5%AE%B9%E5%99%A8%E5%86%85%E9%83%A8%E7%AB%AF%E5%8F%A3%E6%B5%8B%E8%AF%95%E5%B7%B2%E9%80%9A.png)

node1节点，内部端口测试已通

![8.2.node1节点测试内部端口已通](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/8.2.node1%E8%8A%82%E7%82%B9%E6%B5%8B%E8%AF%95%E5%86%85%E9%83%A8%E7%AB%AF%E5%8F%A3%E5%B7%B2%E9%80%9A.png)

node2节点，内部端口测试已通

![8.3.node2节点测试内部端口已通](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/8.3.node2%E8%8A%82%E7%82%B9%E6%B5%8B%E8%AF%95%E5%86%85%E9%83%A8%E7%AB%AF%E5%8F%A3%E5%B7%B2%E9%80%9A.png)

服务器重启测试

![8.4.重启服务器测试](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/8.4.%E9%87%8D%E5%90%AF%E6%9C%8D%E5%8A%A1%E5%99%A8%E6%B5%8B%E8%AF%95.png)

pod正常，业务端口访问正常

![8.5.pod正常，业务端口访问正常](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/8.5.pod%E6%AD%A3%E5%B8%B8%EF%BC%8C%E4%B8%9A%E5%8A%A1%E7%AB%AF%E5%8F%A3%E8%AE%BF%E9%97%AE%E6%AD%A3%E5%B8%B8.png)

删除重建pod,service测试

![9.1.删除重建pod,service](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/9.1.%E5%88%A0%E9%99%A4%E9%87%8D%E5%BB%BApod,service.png)

pod,service,业务访问正常

![9.2.pod,service,业务访问正常](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/k8s%E9%9B%86%E7%BE%A4%E7%8E%AF%E5%A2%83iptables%E6%B5%8B%E8%AF%95/9.2.pod,service,%E4%B8%9A%E5%8A%A1%E8%AE%BF%E9%97%AE%E6%AD%A3%E5%B8%B8.png)

