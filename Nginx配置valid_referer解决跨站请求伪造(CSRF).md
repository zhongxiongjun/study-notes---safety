# Nginx配置valid_referer解决跨站请求伪造(CSRF)

[TOC]

## 漏洞说明

### 漏洞描述

跨站请求伪造（CSRF）。即使是格式正确有效且一致的请求也有可能在用户不知情的情况下发送。例如，如果某个 Web 服务器设计为接收客户机的请求时，没有任何机制来验证该请求是否确实是客户机发送的，那么攻击者就有可能诱导客户机向该 Web 服务器误发请求，而该请求将视为真实请求。这可通过 URL 、图像装入、XMLHttpRequest 等来完成，并可导致数据暴露或意外的代码执行。如果用户当前已登录到受害者站点，请求将自动使用用户的凭证（包括会话 cookie、IP 地址和其他浏览器认证方法）。通过使用此方法，攻击者可伪造受害者的身份，并以其身份提交操作。因此，Web应用程序应该检查所有客户端发起的请求，以发现其不合法的迹象。

跨站请求伪造 Wiki：https://en.wikipedia.org/wiki/Cross-site_request_forgery

### 危害等级

CSRF 漏洞脆弱性的严重程度取决于受影响应用程序的功能。例如，对搜索页面的 CSRF 攻击的严重性低于对转账交易页面的 CSRF 攻击。

### 修复建议

![image-20220221140830350](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221140830350.png)



## 漏洞复现

可以使用 curl 或 BurpSuite 工具携带非法 http_referer 复现测试

### curl测试方法

```bash
curl -h

 -I, --head          Show document info only
 -e, --referer <URL> Referrer URL
```

```bash
curl -e "http://www.baidu.com" -I http://192.168.64.149
```

测试结果：任意修改 referer 信息，服务器都能返回 302 Found

![image-20220221143248101](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221143248101.png)

### BurpSuite测试方法

BurpSuite 下载链接：

https://portswigger.net/burp/releases/professional-community-2022-1-1?requestededition=community

开启代理抓包后，Send to Repeater

![image-20220221144239322](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221144239322.png)

点击 Send 重复发送当前包，下面是未修改的正确 Referer，及其返回状态码：200 OK

![image-20220221144806905](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221144806905.png)

任意修改 Referer 信息，再发送数据包，观察返回状态码。可以看到都能返回：200 OK，即正常请求到 web 页面

![image-20220221145500343](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221145500343.png)



## 漏洞修复

为验证漏洞修复的情况和修复方案的可行性，这里使用 AppScan 对测试网站进行扫描，对比修复前后的结果。

### 修复前扫描测试

![image-20220221150222733](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221150222733.png)

![image-20220221134515231](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221134515231.png)

### 漏洞修复方案

这里采用 AppScan 修复建议中的第7点，修改 nginx.conf，添加 http_referer 头配置

```bash
#       server_name             localhost;

#### http_referer fix
        valid_referers none blocked server_names 127.0.0.1 192.168.64.149 *.baidu.com;
        if ($invalid_referer) {
            return 403;
        }
        
##  语义解析：
##  首先指定valid_referers，即合法的referer值；
##  none：“Referer”字段在请求头中丢失;即允许没有http_refer的请求访问资源；
##  blocked：“Referer”字段在请求头中存在，但其值已被防火墙或代理服务器删除;不以“http://”或“https://”开头的字符串;
##  如果“Referer”请求报头字段值能匹配valid_referers，$invalid_referer为空字符串，否则为“1”；
##  如果匹配到，$invalid_referer为空字符串，if判断语句后的内容不执行；
##  如果匹配不到，$invalid_referer值为1，执行if判断语句后的内容，即返回403跳转页面
##  建议查阅nginx官方文档：http://nginx.org/en/docs/http/ngx_http_referer_module.html
```

![image-20220221220433227](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221220433227.png)

:exclamation::exclamation::exclamation: 需要注意的是，如果已经配置server_name参数，且需要保留，valid_referers 参数需要做调整，否则重启 nginx 时会提示参数冲突（nginx: [emerg] conflicting parameter）。

如下面例子中，允许 referer 的字段值为：127.0.0.1、192.168.64.149、*.baidu.com。最终效果可以理解为 server_name 和 valid_referers 两者互补。

```bash
#       server_name             localhost;

#### http_Host fix
        server_name             127.0.0.1 192.168.64.149;
        if ($http_Host !~* ^127.0.0.1$|^192.168.64.149$) {
                return          403;
        }

#### http_referer fix
        valid_referers none blocked server_names *.baidu.com;
        if ($invalid_referer) {
                return          403;
        }
```

![image-20220222103222376](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220222103222376.png)

重启 Nginx

```bash
nginx -s reload
```

![image-20220221151301024](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221151301024.png)

### 漏洞修复验证

仅当携带 server_name 为 referer 时，服务器返回了 302 Found；

携带其他任意非 server_name 为 referer 的情况下，服务器都返回 403 Forbidden。

- curl 测试结果

![image-20220221151658303](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221151658303.png)

- BurpSuite 测试结果

![image-20220221152600232](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221152600232.png)

![image-20220221152711270](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221152711270.png)

### 修复后扫描测试

![image-20220221152845015](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/valid_referer/image-20220221152845015.png)



**以上，如有错误之处，敬请指正。**



## 参考文章

https://en.wikipedia.org/wiki/Cross-site_request_forgery

http://nginx.org/en/docs/http/ngx_http_referer_module.html

https://www.jianshu.com/p/0de3e5faea0d

http://www.codeforest.cn/article/93

https://www.cnblogs.com/wajika/p/6575656.html