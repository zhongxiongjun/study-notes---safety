# CentosOS7加固测试

[TOC]

## 测试环境说明

| 序号 | IP             | OS         | 测试结果 | 备注                                 |
| ---- | -------------- | ---------- | -------- | ------------------------------------ |
| 1    | 192.168.64.147 | CentOS 7.6 | 正常     | 虚拟机环境，使用root用户登录执行脚本 |



## 一、身份鉴别

### 1.1 密码复杂度检查

检查密码长度和密码是否使用多种字符类型。

```bash
cat /etc/security/pwquality.conf |grep minlen                           #查看当前配置
cat /etc/security/pwquality.conf |grep minclass                         #查看当前配置
sed -i 's/# minlen = 9/minlen = 10/g' /etc/security/pwquality.conf      #最小长度设置为10
sed -i 's/# minclass = 0/minclass = 3/g' /etc/security/pwquality.conf   #最少包含三类字符
cat /etc/security/pwquality.conf |grep minlen                           #检查是否修改成功
cat /etc/security/pwquality.conf |grep minclass                         #检查是否修改成功
```



### 1.2 设置密码失效时间

设置密码失效时间，强制定期修改密码，减少密码被泄漏和猜测风险，使用非密码登陆方式(如密钥对)请忽略此项。

```bash
cat /etc/login.defs |grep PASS_MAX_DAYS |grep -v \#						#默认为99999
chage -l root |grep Maximum												#默认为99999
sed -i 's/PASS_MAX_DAYS\t99999/PASS_MAX_DAYS\t180/g' /etc/login.defs    #普通用户密码失效时间180天
chage --maxdays 180 root                                                #root用户密码失效时间180天
cat /etc/login.defs |grep PASS_MAX_DAYS |grep -v \#
chage -l root |grep Maximum
```

==**root用户的密码失效时间请慎重设置，建议不做修改。若修改，可能导致root用户在下次登录时，提示必须修改密码**==

![image-20220309205751360](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7%E5%8A%A0%E5%9B%BA/image-20220309205751360.png)

如果此项要求必须整改，可以先将root用户密码做一次更改，或通过修改/etc/shadow文件，将root用户密码最后一次修改的时间修改为当天

```bash
#查看root用户密码最后一次修改的时间（以1970年1月1日作为初始时间，不断累加得到的时间差）
[root@localhost ~]# cat /etc/shadow |grep root
root:$6$6AJP9x/s$QSnZp0y/j07Rh7vbxixBpV9dBbQ0kNApDtHCWhEPnzhIJA0geTeeRcXmQi8yvOLrAmHot9Z4jJA/pdxLUsQgD.:18879:0:99999:7:::
[root@localhost ~]# cat /etc/shadow |grep root |awk -F: '{ print $3 }'
18879
#根据时间差，计算出最后一次修改的日期
[root@localhost ~]#  date -d "1970-01-01 18879 days"
Thu Sep  9 00:00:00 CST 2021
#查看当前日期
[root@localhost ~]# date -R
Wed, 09 Mar 2022 21:45:09 +0800
#当前日期-最后一次修改日期，得到时间戳格式的差值，单位为秒
[root@localhost ~]# time=$(($(date +%s -d '2022-03-09') - $(date +%s -d '2021-09-09')))
[root@localhost ~]# echo $time
15638400
#做除法运算得到天数
[root@localhost ~]# echo $[15638400/60/60/24]
181
```

![image-20220309223803579](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7%E5%8A%A0%E5%9B%BA/image-20220309223803579.png)

可以看到，以上例子中，root用户密码最后一次修改的时间是在181天前。因此，

当修改root用户密码失效时间180天时，root用户在下次登录时，提示必须修改密码；

当修改root用户密码失效时间181天时，root用户在下次登录时，并没有提示修改密码，且有警告：您的密码将在今天过期。

**参考：** 编辑/etc/shadow文件，修改root用户密码最后一次修改时间

```bash
#计算出1970年1月1日到今天的日期差
[root@localhost ~]# date -R
Wed, 09 Mar 2022 22:45:56 +0800
[root@localhost ~]# time=$(($(date +%s -d '2022-03-09') - $(date +%s -d '1970-01-01')))
[root@localhost ~]# echo $time
1646784000
[root@localhost ~]# echo $[1646784000/60/60/24]
19060
```

![image-20220309224615171](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7%E5%8A%A0%E5%9B%BA/image-20220309224615171.png)

```bash
#修改/etc/shadow文件中的时间差
cat /etc/shadow |grep root
sed -i '/root/ {s/18879/19060/}' /etc/shadow
cat /etc/shadow |grep root
#修改root用户密码失效时间5天，重新登录进行测试，没有提示必须修改密码，且有警告：您的密码将在5天内过期
chage --maxdays 5 root
```

![image-20220309225918514](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7%E5%8A%A0%E5%9B%BA/image-20220309225918514.png)



### 1.3 检查密码重用是否受限制

强制用户不重用最近使用的密码，降低密码猜测攻击风险。

```bash
cat /etc/pam.d/password-auth |grep "shadow"
cat /etc/pam.d/system-auth |grep "shadow"
sed -i '/shadow/ {s/$/ remember=5/}' /etc/pam.d/password-auth			#行末添加配置参数
sed -i '/shadow/ {s/$/ remember=5/}' /etc/pam.d/system-auth  			#行末添加配置参数
cat /etc/pam.d/password-auth |grep "shadow"
cat /etc/pam.d/system-auth |grep "shadow"
```



### 1.4 确保密码到期警告天数为7或更多

```bash
cat /etc/login.defs |grep PASS_WARN_AGE |grep -v \#						#默认为7
chage -l root |grep warning												#默认为7
sed -i 's/PASS_WARN_AGE\t7/PASS_WARN_AGE\t7/g' /etc/login.defs			#普通用户密码到期告警时间
chage --warndays 7 root                                                 #root用户密码到期告警时间
cat /etc/login.defs |grep PASS_WARN_AGE |grep -v \#
chage -l root |grep warning
```



### 1.5 设置密码修改最小间隔时间

设置密码修改最小间隔时间，限制密码更改过于频繁。

```bash
cat /etc/login.defs |grep PASS_MIN_DAYS |grep -v \#						#默认为0
chage -l root |grep Minimum												#默认为0
sed -i 's/PASS_MIN_DAYS\t0/PASS_MIN_DAYS\t7/g' /etc/login.defs			#普通用户密码修改最小间隔时间
chage --mindays 7 root                                                  #root用户密码修改最小间隔时间
cat /etc/login.defs |grep PASS_MIN_DAYS |grep -v \#
chage -l root |grep Minimum
```



### 1.6 确保root是唯一的UID为0的帐户

```bash
cat /etc/passwd | awk -F: '($3 == 0) { print $1 }'|grep -v '^root$'  #此处只做检查，UID为0帐户不唯一时请手工处理
```



### 1.7 密码输入错误5次后锁定帐号30分钟

防止不停尝试密码，进行爆破攻击。

```bash
cat /etc/pam.d/sshd
sed -i '/#%PAM-1.0/a auth\t   required\tpam_tally2.so deny=5 unlock_time=1800 even_deny_root root_unlock_time=1800' /etc/pam.d/sshd
cat /etc/pam.d/sshd
```



## 二、入侵防范

### 2.1 开启地址空间布局随机化

它将进程的内存空间地址随机化来增大入侵者预测目的地址难度，从而降低进程被成功入侵的风险。

```bash
sysctl -a |grep kernel.randomize_va_space					            #默认配置为2		
sysctl -w kernel.randomize_va_space=2									
```



## 三、文件权限

### 3.1 设置用户权限配置文件的权限

```bash
stat -c %G:%U /etc/passwd /etc/shadow /etc/group /etc/gshadow	 #默认权限为root:root
stat -c %a /etc/passwd /etc/shadow /etc/group /etc/gshadow		 #权限数值低于修改要求中的数值时，可以不做修改
chown root:root /etc/passwd /etc/shadow /etc/group /etc/gshadow
chmod 0644 /etc/group 
chmod 0644 /etc/passwd 
chmod 0400 /etc/shadow 
chmod 0400 /etc/gshadow
stat -c %G:%U /etc/passwd /etc/shadow /etc/group /etc/gshadow
stat -c %a /etc/passwd /etc/shadow /etc/group /etc/gshadow
```



### 3.2 访问控制配置文件的权限设置

```bash
stat -c %G:%U /etc/hosts.allow /etc/hosts.deny	 				 #默认权限为root:root
stat -c %a /etc/hosts.allow /etc/hosts.deny		  				 #权限数值低于修改要求中的数值时，可以不做修改
chown root:root /etc/hosts.allow
chown root:root /etc/hosts.deny
chmod 644 /etc/hosts.deny
chmod 644 /etc/hosts.allow
stat -c %G:%U /etc/passwd /etc/shadow /etc/group /etc/gshadow
stat -c %a /etc/passwd /etc/shadow /etc/group /etc/gshadow
```



## 四、服务配置

### 4.1 确保SSH LogLevel设置为INFO

确保SSH LogLevel设置为INFO,记录登录和注销活动。

```bash
cat /etc/ssh/sshd_config |grep LogLevel
sed -i 's/#LogLevel/LogLevel/g' /etc/ssh/sshd_config							  #取消注释
cat /etc/ssh/sshd_config |grep LogLevel
```



### 4.2 设置SSH空闲超时退出时间

设置SSH空闲超时退出时间,可降低未授权用户访问其他用户ssh会话的风险。

```bash
cat /etc/ssh/sshd_config |grep ClientAlive										  #查看当前配置
sed -i 's/#ClientAliveInterval 0/ClientAliveInterval 600/g' /etc/ssh/sshd_config  #客户端超时时间10分钟
sed -i 's/#ClientAliveCountMax 3/ClientAliveCountMax 2/g' /etc/ssh/sshd_config    #客户端超时次数为2
cat /etc/ssh/sshd_config |grep ClientAlive										  #检查是否修改成功
systemctl restart sshd															  #重启sshd服务
```



### 4.3 SSHD强制使用V2安全协议

```bash
cat /etc/ssh/sshd_config |grep Protocol
echo -e "Protocol 2" >> /etc/ssh/sshd_config 								#文末追加参数
cat /etc/ssh/sshd_config |grep Protocol
```



### 4.4 确保SSH MAXAUTHTRIES设置为3到6之间

设置较低的Max AuthTrimes参数将降低SSH服务器被暴力攻击成功的风险。

```bash
cat /etc/ssh/sshd_config |grep MaxAuthTries
sed -i 's/#MaxAuthTries 6/#MaxAuthTries 4/g' /etc/ssh/sshd_config			#取消注释，设置参数为4
cat /etc/ssh/sshd_config |grep MaxAuthTries
```



### 4.5 禁止SSH空密码用户登录

```bash
cat /etc/ssh/sshd_config |grep PermitEmptyPasswords
sed -i 's/#PermitEmptyPasswords no/PermitEmptyPasswords no/g' /etc/ssh/sshd_config	   #取消注释
cat /etc/ssh/sshd_config |grep PermitEmptyPasswords
```



## 五、安全审计

### 5.1 确保RSYSLOG服务已启用

确保rsyslog服务已启用，记录日志用于审计。

```bash
systemctl status rsyslog |grep Active												   #默认为开启状态
systemctl enable rsyslog
systemctl start rsyslog
systemctl status rsyslog |grep Active
```



## Shell脚本

### check.sh

```bash
#!/bin/bash

echo -e "1.1 密码复杂度检查"
cat /etc/security/pwquality.conf |grep minlen                           #查看当前配置
cat /etc/security/pwquality.conf |grep minclass                         #查看当前配置
echo --------------------------------------

echo -e "1.2 设置密码失效时间"
cat /etc/login.defs |grep PASS_MAX_DAYS |grep -v \#			
chage -l root |grep Maximum
echo --------------------------------------

echo -e "1.3 检查密码重用是否受限制"
cat /etc/pam.d/password-auth |grep "shadow"
cat /etc/pam.d/system-auth |grep "shadow"
echo --------------------------------------

echo -e "1.4 确保密码到期警告天数为7或更多"
cat /etc/login.defs |grep PASS_WARN_AGE |grep -v \# 
chage -l root |grep warning
echo --------------------------------------

echo -e "1.5 设置密码修改最小间隔时间"
cat /etc/login.defs |grep PASS_MIN_DAYS |grep -v \#
chage -l root |grep Minimum
echo --------------------------------------

echo -e "1.6 确保root是唯一的UID为0的帐户"
#此处只做检查，UID为0帐户不唯一需手工处理
user=`cat /etc/passwd | awk -F: '($3 == 0) { print $1 }'|grep -v '^root$'`	
if [ -z "$user" ]; then
    echo -e "root是唯一的UID为0帐户"
else
    echo -e "\033[31;40mroot不是唯一的UID为0账户，UID为0账户有:`cat /etc/passwd | awk -F: '($3 == 0) { printf $1 " " }'`\033[0m"
fi
echo --------------------------------------

echo -e "1.7 密码输入错误5次后锁定帐号30分钟"
cat /etc/pam.d/sshd
echo --------------------------------------

echo -e "2.1 开启地址空间布局随机化"
sysctl -a |grep kernel.randomize_va_space				 		 #默认配置为2		
echo --------------------------------------

echo -e "3.1 设置用户权限配置文件的权限"
stat -c %G:%U /etc/passwd /etc/shadow /etc/group /etc/gshadow	 #默认权限为root:root
stat -c %a /etc/passwd /etc/shadow /etc/group /etc/gshadow		 #权限数值低于修改要求中的数值时，可以不做修改
echo --------------------------------------

echo -e "3.2 访问控制配置文件的权限设置"
stat -c %G:%U /etc/hosts.allow /etc/hosts.deny	 				 #默认权限为root:root
stat -c %a /etc/hosts.allow /etc/hosts.deny		  				 #权限数值低于修改要求中的数值时，可以不做修改
echo --------------------------------------

echo -e "4.1 确保SSH LogLevel设置为INFO"
cat /etc/ssh/sshd_config |grep LogLevel
echo --------------------------------------

echo -e "4.2 设置SSH空闲超时退出时间"
cat /etc/ssh/sshd_config |grep ClientAlive						  #查看当前配置
echo --------------------------------------

echo -e "4.3 SSHD强制使用V2安全协议"
cat /etc/ssh/sshd_config |grep Protocol
echo --------------------------------------

echo -e "4.4 确保SSH MAXAUTHTRIES设置为3到6之间"
cat /etc/ssh/sshd_config |grep MaxAuthTries
echo --------------------------------------

echo -e "4.5 禁止SSH空密码用户登录"
cat /etc/ssh/sshd_config |grep PermitEmptyPasswords
echo --------------------------------------

echo -e "5.1 确保RSYSLOG服务已启用"
systemctl status rsyslog |grep Active							   #默认为开启状态
echo --------------------------------------
```



### reinforce.sh

```bash
#!/bin/bash
echo -ne "1.1 密码复杂度检查"
sed -i 's/# minlen = 9/minlen = 10/g' /etc/security/pwquality.conf      #最小长度设置为10
sed -i 's/# minclass = 0/minclass = 3/g' /etc/security/pwquality.conf   #最少包含三类字符
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "1.2 设置密码失效时间"
now=`date '+%Y-%m-%d'`													#当前日期
diff=$(($(date +%s -d $now) - $(date +%s -d '1970-01-01')))				#与1970-01-01的差值时间戳
newtime=$((diff/60/60/24))												#转换成天数
time=`cat /etc/shadow |grep root |awk -F: '{ print $3 }'`				#当前记录的天数
sed -i "/root/s#$time#$newtime#" /etc/shadow							#替换数值
sed -i 's/PASS_MAX_DAYS\t99999/PASS_MAX_DAYS\t180/g' /etc/login.defs    #普通用户密码失效时间180天
chage --maxdays 180 root                                                #root用户密码失效时间180天
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "1.3 检查密码重用是否受限制"
sed -i '/shadow/ {s/$/ remember=5/}' /etc/pam.d/password-auth			#行末添加配置参数
sed -i '/shadow/ {s/$/ remember=5/}' /etc/pam.d/system-auth  			#行末添加配置参数
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "1.4 确保密码到期警告天数为7或更多"
sed -i 's/PASS_WARN_AGE\t7/PASS_WARN_AGE\t7/g' /etc/login.defs			#普通用户密码到期告警时间
chage --warndays 7 root                                                 #root用户密码到期告警时间
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "1.5 设置密码修改最小间隔时间"
sed -i 's/PASS_MIN_DAYS\t0/PASS_MIN_DAYS\t7/g' /etc/login.defs			#普通用户密码修改最小间隔时间
chage --mindays 7 root                                                  #root用户密码修改最小间隔时间
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "1.6 确保root是唯一的UID为0的帐户"
#此处只做检查，UID为0帐户不唯一时请手工处理
user=`cat /etc/passwd | awk -F: '($3 == 0) { print $1 }'|grep -v '^root$'`
if [ -z "$user" ]; then
    echo -e "\033[32;40m --Done\033[0m"
else
    echo -e "\033[31;40m\nroot不是唯一的UID为0账户，UID为0账户有:`cat /etc/passwd | awk -F: '($3 == 0) { printf $1 " " }'`\033[0m"
fi
echo --------------------------------------

echo -ne "1.7 密码输入错误5次后锁定帐号30分钟"
sed -i '/#%PAM-1.0/a auth\t   required\tpam_tally2.so deny=5 unlock_time=1800 even_deny_root root_unlock_time=1800' /etc/pam.d/sshd
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "2.1 开启地址空间布局随机化"
sysctl -w kernel.randomize_va_space=2 >/dev/null 2>&1 									
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "3.1 设置用户权限配置文件的权限"
chown root:root /etc/passwd /etc/shadow /etc/group /etc/gshadow
chmod 0644 /etc/group 
chmod 0644 /etc/passwd 
chmod 0400 /etc/shadow 
chmod 0400 /etc/gshadow
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "3.2 访问控制配置文件的权限设置"
chown root:root /etc/hosts.allow
chown root:root /etc/hosts.deny
chmod 644 /etc/hosts.deny
chmod 644 /etc/hosts.allow
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "4.1 确保SSH LogLevel设置为INFO"
sed -i 's/#LogLevel/LogLevel/g' /etc/ssh/sshd_config							  #取消注释
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "4.2 设置SSH空闲超时退出时间"
sed -i 's/#ClientAliveInterval 0/ClientAliveInterval 600/g' /etc/ssh/sshd_config  #客户端超时时间10分钟
sed -i 's/#ClientAliveCountMax 3/ClientAliveCountMax 2/g' /etc/ssh/sshd_config    #客户端超时次数为2
systemctl restart sshd															  #重启sshd服务
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "4.3 SSHD强制使用V2安全协议"
echo -e "Protocol 2" >> /etc/ssh/sshd_config 								#文末追加参数
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "4.4 确保SSH MAXAUTHTRIES设置为3到6之间"
sed -i 's/#MaxAuthTries 6/#MaxAuthTries 4/g' /etc/ssh/sshd_config			#取消注释，设置参数为4
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "4.5 禁止SSH空密码用户登录"
sed -i 's/#PermitEmptyPasswords no/PermitEmptyPasswords no/g' /etc/ssh/sshd_config	   #取消注释
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------

echo -ne "5.1 确保RSYSLOG服务已启用"
systemctl enable rsyslog
systemctl start rsyslog
echo -e "\033[32;40m --Done\033[0m"
echo --------------------------------------
```

### 脚本测试

检查脚本：check.sh

![image-20220310143909822](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7%E5%8A%A0%E5%9B%BA/image-20220310143909822.png)

加固脚本：reinforce.sh

![image-20220310144032795](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7%E5%8A%A0%E5%9B%BA/image-20220310144032795.png)

shadow文件修改结果

![image-20220310144750949](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7%E5%8A%A0%E5%9B%BA/image-20220310144750949.png)

加固前后的检查结果比对

![image-20220310144355226](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7%E5%8A%A0%E5%9B%BA/image-20220310144355226.png)

![image-20220310144447317](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7%E5%8A%A0%E5%9B%BA/image-20220310144447317.png)

多个账户UID为0测试

![image-20220310145812167](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7%E5%8A%A0%E5%9B%BA/image-20220310145812167.png)

![image-20220310150809847](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/CentOS7%E5%8A%A0%E5%9B%BA/image-20220310150809847.png)



## 参考文章

[Linux /etc/shadow（影子文件）内容解析（超详细）](http://c.biancheng.net/view/840.html)

[linux shell date 时间运算以及时间差计算方法](https://www.cnblogs.com/zendu/p/4987927.html)

[多个UID为0的用户如何实现root用户的免密](https://blog.csdn.net/u010383467/article/details/118479348?utm_term=linux%E5%88%9B%E5%BB%BAuid%E4%B8%BA0%E7%9A%84%E7%94%A8%E6%88%B7&utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduweb~default-1-118479348&spm=3001.4430)
