## SSL自签证书不可信规避方法

使用SSL自签证书，通常会被扫描器扫描出自签证书不可信的中危漏洞，扫描报告中修复建议一般都是：使用第三方机构证书。但对于自开发且仅限内部使用的应用系统，若为其购买第三方证书，需要考虑项目成本和周期，就有点不切实际了。

可以根据实际环境，适当选择关闭对应服务，或添加防火墙规则进行访问限制，实现规避。



**参考案例**

![image-20211202160855549](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/SSL%E8%87%AA%E7%AD%BE%E8%AF%81%E4%B9%A6%E4%B8%8D%E5%8F%AF%E4%BF%A1%E8%A7%84%E9%81%BF%E6%96%B9%E6%B3%95/image-20211202160855549.png)

![image-20211202160733714](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/SSL%E8%87%AA%E7%AD%BE%E8%AF%81%E4%B9%A6%E4%B8%8D%E5%8F%AF%E4%BF%A1%E8%A7%84%E9%81%BF%E6%96%B9%E6%B3%95/image-20211202160733714.png)

![image-20211202162006315](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/SSL%E8%87%AA%E7%AD%BE%E8%AF%81%E4%B9%A6%E4%B8%8D%E5%8F%AF%E4%BF%A1%E8%A7%84%E9%81%BF%E6%96%B9%E6%B3%95/image-20211202162006315.png)

漏扫报告中可以看到，使用自签证书的端口有：

- tcp/5000
- tcp/6443
- tcp/9100

确认端口对应的服务如下：

![image-20211202162835612](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/SSL%E8%87%AA%E7%AD%BE%E8%AF%81%E4%B9%A6%E4%B8%8D%E5%8F%AF%E4%BF%A1%E8%A7%84%E9%81%BF%E6%96%B9%E6%B3%95/image-20211202162835612.png)

![img](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/SSL自签证书不可信规避方法/%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_1638435215188.png)

![image-20211202173834413](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/SSL%E8%87%AA%E7%AD%BE%E8%AF%81%E4%B9%A6%E4%B8%8D%E5%8F%AF%E4%BF%A1%E8%A7%84%E9%81%BF%E6%96%B9%E6%B3%95/image-20211202173834413.png)

![image-20211202173517539](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/SSL%E8%87%AA%E7%AD%BE%E8%AF%81%E4%B9%A6%E4%B8%8D%E5%8F%AF%E4%BF%A1%E8%A7%84%E9%81%BF%E6%96%B9%E6%B3%95/image-20211202173517539.png)

| 端口 | 服务                  | 作用                                                         |
| ---- | --------------------- | ------------------------------------------------------------ |
| 5000 | registry              | 仓库服务，访问镜像仓库时需要用到                             |
| 6443 | kube-apiserver        | apiserver提供了k8s各类资源对象（pod,RC,Service等）的增删改查及watch等HTTP Rest接口，是整个系统的数据总线和数据中心 |
| 9100 | service/node-exporter | prometheus插件，获取机器的硬件信息                           |

1. 关闭服务端口

registry服务可以关闭，在需要访问仓库拉取或上传镜像时开启即可

```bash
systemctl stop docker-distribution 
```

2. 添加硬件防火墙规则

结合实际网络拓扑，若项目现场有硬件防火墙，可以直接在防火墙上添加规则，禁止无关IP访问服务端口，前提是访问的流量必须经过该防火墙。这里的5000和9100端口使用此方法实现

2. 设置iptables规则

k8s 单节点部署的业务环境，可以通过添加 iptables 规则，拒绝所有IP访问 kube-apiserver 的6443端口；集群部署的业务环境，iptables规则需要先添加允许集群IP访问6443端口的规则，再拒绝所有IP访问6443端口

```bash
iptables -A INPUT -p TCP --dport 6443 -j REJECT
service iptables save
iptables -L -nvx --line-numbers
```

下图是单节点部署环境下的 rules

![image-20211202173024302](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/SSL%E8%87%AA%E7%AD%BE%E8%AF%81%E4%B9%A6%E4%B8%8D%E5%8F%AF%E4%BF%A1%E8%A7%84%E9%81%BF%E6%96%B9%E6%B3%95/image-20211202173024302.png)

