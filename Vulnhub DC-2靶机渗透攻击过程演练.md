# Vulnhub DC-2靶机渗透攻击过程演练

## DC-2介绍

与DC-1相同，[DC-2](https://www.vulnhub.com/entry/dc-2,311/)靶机也有5个flag，需要注意的是，要添加host才能访问DC-2的web

![image-20211129154156487](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211129154156487.png)

![image-20211129155001587](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211129155001587.png)

![image-20211223112117781](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223112117781.png)



## 环境准备

下载DC-2靶机导入VMware，选择和Kali同一网络适配器，DHCP获取IP即可

![image-20211130115056596](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211130115056596.png)

![image-20211130115308378](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211130115308378.png)



## 渗透过程

### 信息收集

扫描存活主机，得到目标靶机的IP

```bash
nmap -sP 192.168.64.0/24
```

![image-20211223104049713](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223104049713.png)

扫描端口，初步收集信息

```bash
nmap -T4 -sC -sV 192.168.64.128
```

![image-20211223104332795](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223104332795.png)

添加host，访问web测试

![image-20211223120434275](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223120434275.png)

主页上可以看到是WordPress的CMS，拿到flag1，提示可以用 [cewl](https://www.kali.org/tools/cewl/) 生成字典爆破密码，登录后去找下一个flag

![image-20211223120733378](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223120733378.png)

从[WordPress官网](https://wordpress.org/support/article/how-to-install-wordpress/)的安装手册上找一些有用的信息，尝试通过 http://example.com/wp-admin/install.php 目录访问测试

![image-20211223123257819](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223123257819.png)

访问 http://dc-2/wp-admin/install.php 提示已经安装，点击下方login按钮即可跳转到后台登录页面

![image-20211223123638260](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223123638260.png)

跳转登录页面 http://dc-2/wp-login.php

![image-20211223123835511](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223123835511.png)

在官网安装手册的最后一步中，需要输入用户名密码，这里的示例中的username是admin，而大多数系统或设备的默认管理员用户名也是admin

![image-20211223124104460](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223124104460.png)

我们可以通过观察登录时的回显信息，验证系统中是否存在admin用户。输入用户名为`1`时的回显：**ERROR**: Invalid username.（用户不存在）

![image-20211223124755027](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223124755027.png)

输入用户名为`admin`时的回显：**ERROR**: The password you entered for the username **admin** is incorrect.（密码错误）

![image-20211223124945643](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223124945643.png)

### 爆破密码

使用cewl生成字典，生成了260个密码

```bash
cewl -d 3 -aev -with-numbers -w passwd.txt http://dc-2/wp-login.php
cat passwd.txt |wc -l
```

![image-20211223142653704](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223142653704.png)

![image-20211223142747055](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223142747055.png)

BurpSuite抓包，设置payload

![image-20211223143806371](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223143806371.png)

加载cewl生成的密码字典

![image-20211223144004100](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223144004100.png)

尝试爆破admin的密码，跑完字典后，排序Length的值都一样，显然是没有爆破出admin的密码

![image-20211223151810764](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211223151810764.png)

根据上述的登录回显测试，可以看出admin用户是存在的，但未必只有一个用户，正如作者在flag1中的提示：密码越多越好，但有时你不可能拿得所有的密码。登录一个用户用于找到下一个flag。如果你找不到，就用另一个登录。

如果网站对用户登录失败的回显有区分，而且登录又不需要验证码，就会导致很容易被枚举出用户名。在kali上已经集成了一款工具：[wpscan](https://www.kali.org/tools/wpscan/)，可以枚举wordpress的用户名，扫描目录和漏洞等，下面就使用wpscan尝试枚举用户

![image-20211224101239482](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211224101239482.png)

```bash
wpscan -e --url http://dc-2
```

![image-20211224101359145](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211224101359145.png)

枚举出三个用户名：admin、jerry、tom

![image-20211224101612338](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211224101612338.png)

继续使用BurpSuite爆破另一个用户名：jerry

![image-20211224102029310](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211224102029310.png)

最终发现一个爆破成功的密码：adipiscing

![image-20211224104848345](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211224104848345.png)

感觉BurpSuite爆破太慢，效率太低，可以修改线程数。或者直接使用wpscan爆破

```bash
wpscan -U admin,jerry,tom -P passwd.txt --url http://dc-2
```

![image-20211224105147585](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211224105147585.png)

得到两个密码：jerry/adipiscing、tom/parturient

![image-20211224105633999](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211224105633999.png)

尝试登录web：登录成功

![image-20211224105932210](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211224105932210.png)

### 寻找突破点

在Pages中找到flag2

![image-20211224110201090](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211224110201090.png)

直接编辑flag2的page内容添加php木马肯定是不可行的，在编辑框内的php语言并不会被解析

![image-20211227101758821](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211227101758821.png)



尝试寻找上传漏洞，直接上传php文件提示文件类型不支持，上传jpeg和mp4格式则是提示没有权限

![image-20211227102521722](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211227102521722.png)

再用wpscan扫描漏洞，看有没有什么新发现

```bash
wpscan --url http://dc-2
```

![image-20211227152713048](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211227152713048.png)

这里黄色感叹号部分是着重关注的点，使用了停止维护更新的主题twentyseventeen，网上搜索了一下相关漏洞，有个攻击的[例子](https://www.pianshen.com/article/1000275253/)，但似乎是要用admin账户才能修改外观主题，而我并没有拿到admin用户的密码。只能另寻其他路子，用Nessus和awvs登录扫描出很多漏洞，但最终没有找到利用成功的方法

![image-20211230151742417](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230151742417.png)

![image-20211230151553671](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230151553671.png)

![image-20211230151647565](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230151647565.png)

就在无计可施之时，搜了几篇大佬写的DC-2渗透测试文章，”拿来主义“直接套用了，重在于理解以及学习渗透的思路和方法。

其实DC-2靶机作者的小心思都很明显了，在flag2的提示中：如果你不能使用WordPress，也不能走捷径，还有另一种方法。希望你找到另一个入口。jerry和tom这两个是低权限的用户，可能无法利用wordpress的漏洞的，提示找另外的入口。而我在前面收集信息时，nmap只扫了常用端口，使用全端口去扫描就会发现，作者开放了一个非常规端口7744/tcp，并且是ssh的，突破口原来是这里。

```bash
nmap -T4 -sC -sV -p1-65535 192.168.64.128      # 全端口扫描
```

![image-20211227091756239](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211227091756239.png)

用wpscan枚举出的用户名和cewl生成的密码字典，使用hydra尝试爆破ssh，发现tom/parturient登录成功，也就是说和web用户一样的密码

```bash
hydra -L user.txt -P passwd.txt -t 30 192.168.64.128 -s 7744 ssh
```

![image-20211230151231670](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230151231670.png)

ssh连接到靶机，`ls` 便可看到flag3

```bash
ssh tom@192.168.64.128 -p 7744
```

![image-20211230155425214](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230155425214.png)

vi 编辑器查看flag3.txt的内容，提示：可怜的老汤姆总是追赶杰瑞。也许他应该为他所造成的压力而放弃。

![image-20211230160022969](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230160022969.png)

![image-20211230160238564](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230160238564.png)

### 绕过&提权

tom用户有限制，很多命令无法使用，要想办法提权了，跟据flag3提示需要切换到jerry用户，[rbash](https://cloud.tencent.com/developer/article/1680551) 是一种受限制bash，有多种绕过的方法

![image-20211230171433124](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230171433124.png)

```bash
vi 11                             #打开vi编辑器
:set shell=/bin/bash              #在末行模式输入，将shell设置为/bin/bash
:shell			                  #在末行模式输入，执行shell
```

![image-20211230181437693](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230181437693.png)

![image-20211230181843746](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230181843746.png)

![image-20211230181507857](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230181507857.png)

此时的shell则是正常的bash shell，虽然也是有限制，但可以cd到jerry的家目录，使用less命令查看到flag4.txt的内容

```bash
cd /home/jerry
less flag4.txt
```

![image-20211230184014095](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230184014095.png)

![image-20211230184311227](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230184311227.png)

为了方便继续找到最后一个flag，先提权

```bash
echo $PATH                         # 当前的环境变量为：/home/tom/usr/bin
export PATH=$PATH:/bin:/usr/bin    # 修改后环境变量为：/home/tom/usr/bin:/bin:/usr/bin

## linux的环境变量中，PATH是定义路径的变量,可以添加多个路径，以冒号做为目录间的分割符
## 上面修改后的环境变量中，其实就是多添加了root用户的两个环境变量路径，借用了root用户的环境变量，便有了更多的命令执行权限

whoami       # 测试whoami查询当前用户，命令可用，当前用户为tom
su jerry	 # 测试su切换jerry用户，命令可用，尝试用前面wpscan爆破的密码jerry/adipiscing登录成功
whoami	     # 再次查看当前用户，已经成功切换成jerry
```

![image-20211230213933504](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230213933504.png)

jerry用户则可以直接使用cat命令查看flag4.txt

![image-20211230221523910](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230221523910.png)

进一步提升权限

```bash
find / -perm -u=s -type f 2>/dev/null       # 查找具有SUID权限的文件
sudo -l       # 查看当前用户可提升权限执行的程序，发现jerry用户可以不需要root密码使用git命令
```

![image-20211230225243886](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230225243886.png)

利用git提升权限

```bash
sudo git help config    # 借用root身份，进入git的help config
!/bin/bash              # 末行模式输入!/bin/bash打开一个新的bash shll
## 因为此时的身份为root，打开新的bash shell自然也就是root权限的shell
```

![image-20211230225613266](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230225613266.png)

![image-20211230225657759](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230225657759.png)

提权到root用户后，在root家目录下找到final-flag

![image-20211230225809543](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230225809543.png)

### 理解&验证

#### 理解sudo提权

为了加深理解sudo提权，下面是在kali上做个对比演示

kali用户vi编辑器的末行模式输入 `!/bin/bash` 中打开新的shell，身份没有变化

![image-20211230232350884](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230232350884.png)

![image-20211230232407110](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230232407110.png)

sudo借用root身份打开vi编辑器的末行模式输入 `!/bin/bash` 中打开新的shell

![image-20211230232633919](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230232633919.png)

![image-20211230232542899](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230232542899.png)

此时身份已经变成root

![image-20211230232701030](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230232701030.png)

输入exit退出时当前的shell时，可以看到需要先退出到vi编辑器，再从编辑器中退出，用户身份也退回到kali了

![image-20211230233047358](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230233047358.png)

前面过程中并没有输入验证root用户的密码，su切换用户则是需要root密码的

![image-20211230233351520](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211230233351520.png)

#### 验证twentyseventeen主题漏洞

为了验证前面提到的twentyseventeen主题漏洞是否能利用，尝试先找到或重置admin用户的密码，参考这篇文章的方法：[点此跳转](https://www.jianshu.com/p/c9af273456af)

下面这种方法看似是改成功了，使用新密码登录时也没有提示密码错误，但是却一直登录不上web管理后台

![image-20211231103342954](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231103342954.png)

![image-20211231103254212](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231103254212.png)

只能尝试修改数据库，在wordpress的目录下找到一个wp-config.php配置文件，里面配置有数据库的相关信息

![image-20211231104926847](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231104926847.png)

![image-20211231104634902](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231104634902.png)

使用配置文件中的数据库信息登录mysql

```mysql
mysql -uwpadmin -p4uTiLL                      # 登录mysql
use wordpressdb;                              # 使用wordpressdb数据库
show tables;                                  # 显示表
select * from wp_users;                       # 查询wp_users表中的数据
```

![image-20211231110643697](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231110643697.png)

![image-20211231110719911](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231110719911.png)

![image-20211231110746712](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231110746712.png)

将admin的密码字段替换成jerry用户的值，但是在刷新时提示没有权限，退出重新登录mysql发现值没有更改

```mysql
update wp_users set user_pass="$P$BRCcbpudGlBukTwA7kJsb.rafAL4il." where id=1;
flush privileges;
```

![image-20211231124252193](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231124252193.png)

![image-20211231124356751](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231124356751.png)

最后实在没招，准备换其他的方法，先复原修改过的配置，将前面主题修改密码添加的内容注释掉

![image-20211231124802841](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231124802841.png)

此时，惊奇的发现，密码已经改成Hahagotit了

![image-20211231125324886](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231125324886.png)

原来前面所用的改密方法是可行的，只是我操作不当。添加参数后只需登录一次即可对参数注释或删除，否则就会导致每次登录都执行密码重置的参数，循环执行。详细说明请参考[wordpress官方文档](https://developer.wordpress.org/reference/functions/wp_set_password/)

编辑404.php，但提示没有权限保存修改

![image-20211231175429344](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231175429344.png)

在bash中查看这个文件，其他用户是只读权限的

![image-20211231180002625](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231180002625.png)

加个写的权限测试，页面没有变化

![image-20211231180404038](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231180404038.png)

直接给777权限测试，刷新页面后可以看到底下的保存按钮了

![image-20211231180650142](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231180650142.png)

![image-20211231180746127](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231180746127.png)

将webshell写入404.php中保存

![image-20211231181020046](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231181020046.png)

webshell链接成功

![image-20211231181323722](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-2%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211231181323722.png)

这个主题漏洞的利用条件虽然较为苛刻，即：系统中的文件权限设置不当，admin用户密码被泄露或成功爆破，一旦利用成功，造成的危害是很大的



## 总结

1、在渗透的实战过程中，尽可能多的收集信息，然后利用一切能利用的点去挖掘，比如在本次演练中的nmap全端口扫描，而不只是常端口扫描。

2、密码爆破原理虽然简单，但却很常见且实用。要善用密码字典，“只要你的字典足够强大，就没有爆破不了的密码”。web用户和系统用户虽然可以是无相关的，但网络管理员为了方便自己记忆和管理，习惯性地用了同一套用户名密码，这在实际环境中也是非常常见的，比如企业内部的统一身份认证登录。

3、多查阅参考官方文档和用户手册是最为可靠的。



## 参考文章

RBash - 受限的Bash绕过：https://cloud.tencent.com/developer/article/1680551

受限的Linux shell绕过技术：https://fireshellsecurity.team/restricted-linux-shell-escaping-techniques/

DC2- WordPress 靶机：https://www.icode9.com/content-4-834674.html

WordPress忘记密码怎么办？7种方法帮你找回WordPress外贸网站登录密码：https://www.jianshu.com/p/c9af273456af

wp_set_password重置密码：https://developer.wordpress.org/reference/functions/wp_set_password/
