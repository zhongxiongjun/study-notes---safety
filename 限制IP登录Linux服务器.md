# 限制IP登录Linux服务器

在安全运维管理和基线合规要求下，通常需要限制 IP登录服务器，但在项目实际环境中，考虑到业务的影响，可能无法直接通过开启防火墙来实现，下面整理了几种方法及测试过程，以供参考。

==**操作有风险，请谨慎！！！**==

*客户要求做白盒扫描时，可以综合考虑实际情况，使用限制 IP登录的方式进行规避*

| 序号 |     测试IP     |    说明     |
| :--: | :------------: | :---------: |
|  1   |  192.168.64.1  |   客户端1   |
|  2   | 192.168.64.129 |   客户端2   |
|  3   | 192.168.64.131 | Linux服务器 |

![image-20211130190013690](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130190013690.png)

![image-20211130191858976](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130191858976.png)



## 方法一：TCP Wrappers服务访问控制
**注意：** TCPwrappers模块在高版本openssh中被移除，以下测试环境中centos7的openssh7.4可以实现

```bash
vi /etc/hosts.allow           	 	 # 编辑hosts.allow

sshd:192.168.64.1				     # 添加一行，允许指定的IP登录
```

![image-20211130191634643](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130191634643.png)

```bash
vi /etc/hosts.deny           	 	 # 编辑hosts.deny

sshd:ALL     						 # 添加一行，拒绝所有IP登录
```

![image-20211130175900510](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130175900510.png)

测试结果：

![image-20211130192057222](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130192057222.png)

![image-20211130192120315](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130192120315.png)



## 方法二：修改sshd_config配置文件实现

```bash
vi /etc/ssh/sshd_config				 # 编辑sshd_config

ALLowUsers      *@192.168.64.129	 # 添加允许登录的IP，*代表任意用户
#DenyUsers      *@192.168.64.1		 # 拒绝登录IP，与ALLowUsers二者选其一即可
```

![image-20211130200831242](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130200831242.png)

```bash
systemctl restart sshd				 # 重启sshd服务
```

![image-20211130194026208](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130194026208.png)

测试结果：

![image-20211130194208392](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130194208392.png)

![image-20211130195347659](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130195347659.png)



## 方法三：防火墙规则限制

### firewalld

```bash
# 开启firewalld，并设置开机自启
systemctl enable firewalld && systemctl restart firewalld && systemctl status firewalld
```

![image-20211130201644093](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130201644093.png)

```bash
# 添加允许规则,放行指定IP访问服务器22端口
firewall-cmd --permanent --add-rich-rule="rule family="ipv4" source address="192.168.64.1/32" port protocol="tcp" port="22" accept"
# 重载防火墙规则
firewall-cmd --reload
# 查看防火墙规则
firewall-cmd --list-all
```

![image-20211130202801132](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130202801132.png)

```bash
# 添加拒绝规则，拒绝指定IP访问服务器22端口
firewall-cmd --permanent --add-rich-rule="rule family="ipv4" source address="192.168.64.129/32" port protocol="tcp" port="22" drop"
# 重载防火墙规则
firewall-cmd --reload
# 查看防火墙规则
firewall-cmd --list-all
```

![image-20211130203355783](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130203355783.png)

测试结果：

![image-20211130203530641](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130203530641.png)

![image-20211130203557493](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130203557493.png)

```bash
# 删除防火墙规则
firewall-cmd --permanent --remove-rich-rule="rule family="ipv4" source address="192.168.64.1" port protocol="tcp" port="22" accept"
firewall-cmd --reload
firewall-cmd --list-all
```

### iptables

CentOS7默认的防火墙不是iptables，而是firewall

```bash
# 停止firewalld服务
systemctl stop firewalld
# 禁用firewalld服务
systemctl mask firewalld
# 安装iptables-services
yum install -y iptables-services
# 开启iptables，并设置开机自启
systemctl enable iptables && systemctl start iptables && systemctl status iptables
```

![image-20211130214808000](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130214808000.png)

```bash
# 添加允许规则，允许范围IP访问22端口
iptables -A INPUT -m iprange --src-range 192.168.64.120-192.168.64.130 -p tcp --dport 22 -j ACCEPT
# 添加允许规则，允许本机访问22端口
iptables -A INPUT -s 127.0.0.1 -p tcp --dport 22 -j ACCEPT
# 添加拒绝规则，拒绝其他所有访问22端口
iptables -A INPUT -p TCP --dport 22 -j REJECT
```

![image-20211130214900276](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130214900276.png)

```bash
service iptables save 				     # 保存iptables规则
iptables -L -n							 # 查看iptables规则
```

![image-20211130214936875](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130214936875.png)

**在 iptables 的 INPUT 链中默认有两条规则：**

1. 允许所有IP访问服务器22端口（下图中的第4条规则）
2. 拒绝其他所有访问（下图中的第5条规则）

```bash
iptables -L -nvx --line-numbers 		# 查看iptables规则，并列出规则的编号
```

![image-20211130215632576](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130215632576.png)

```bash
iptables -D INPUT 4						# 删除INPUT中的第4条规则
iptables -L -nvx --line-numbers			# 再次查看iptables规则
iptables -D INPUT 4						# 删除INPUT中的第4条规则
service iptables save					# 保存iptables规则
```

![image-20211130222032450](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130222032450.png)

![image-20211130222140801](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130222140801.png)

测试结果：

![image-20211130222551058](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130222551058.png)

![image-20211130222632265](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/%E9%99%90%E5%88%B6%20IP%20SSH%E7%99%BB%E5%BD%95Linux%E6%9C%8D%E5%8A%A1%E5%99%A8/image-20211130222632265.png)